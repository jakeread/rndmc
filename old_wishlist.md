## From John / Ryan / Tom

PNG / Hairline no bueno -> no direct hairline instructions
rhino.black.png
rhino.png               inversion issues / normalize A values?

## Function
Data structure assembly... merge objects into array
    usecase -> image combine, want to combine n number of images
    input to 'combine' should be singular, of type array-of-images
Save PNG

## Debug
TOP PICK reload mods from server in existing graph sketch
-> this as experiment on all mods through some global level, to add 'refresh' button
    or do development tools in-mods so that you can IDE while u IDE :/
2ND PICK sniffer / forwarding (fires events) tool that is type-agnostic & displays intelligently
3RD - RGBA Washer. Or better understanding. WTF
mods.log('message') reports mod id, time fired ...
rgb preview canvases get friendly packaged class, checkered background 2 c transparency

## UI
scroll / zoom / pan
pipe inspektor
open-mod on double-click and text
copy / paste / duplicate
drag pipe, no click
full-size canvases open up scaled to screen, show scale factor, drag zoom ...

## Nitpick
event does not fire when 'line loaded' -> have to re-select svg
distance transform 'aw snaps' when given un-thresholded rgba values

### KiCad -> PNG via MODS
area fill
do full programs
commit programs

on mods, api architecture ... open distributed operating systems
 dna carrying programs, hairballs
 APIs handing eachother their available calls: 
 mods-type program is UI for finding, viewing, editing graphs, all calls are object calls, JSON, can carry programs to execute (hard with C) 
 where the level shifts: kinematic modules -> design | hardware
 js programs -> ? | hardware | C
 design of components -> design of assemblies -> | hardware

 main beef is separation UI -> Business. Bug or Feature? UI should be program whose job it is to represent software. To mix is messy, results in code that is ultimately unreadable: presents too much complexity, not really accessible.

 UI program starts at UI and polls through some node to programs elsewhere...  ?

 and a desire for easily included libraries: 3js, custom maths, etc