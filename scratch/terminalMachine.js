// a run file: the machine in one js scratchpad

// motors,
var xMotor = createStepper()
xMotor.route = [0,0] // apa route to hardware

var ylMotor = createStepper()
ylMotor.route = [0,1]

var yrMotor = createStepper()
yrMotor.route = [0,2]

var zMotor = createStepper()
zMotor.route = [0,3]

var spindle = createBLDC()
spindle.route = [0,4]


// a lookahead planner, eats movement instructions and makes smooth trapezoids, tracks motors and their state
var planner = createPlanner()
// will listen for these move axis enumerators, motion planning done with pt arrays in this order in cartesian space
planner.axisConfig = ['X', 'Y', 'Z']
// will output to these actuators, should be able to write expressions here for kinematics: corexy, h-bot etc
planner.actuatorConfig = {
	X: X * 200,
	YL: Y * 300,
	YR: -Y * 300,
	Z: Z * 200
}
// would have defaults, but
planner.accelRate = 250 // units/s
planner.jd = 0.5 // units for radius of imaginary junction


// an input
var terminal = createTerminal()
terminal.charWidth = 120
terminal.linesTall = 64

// to parse gcode - hella stateful!
var gcodeParser = createGcodeParser()


// now we hook up:

// terminal puts characters into the gcode parser, which writes move commands 

terminal.outputs.newline.attach(gcodeParser.inputs.parse)
gcodeParser.outputs.newmove.attach(planner.inputs.newMove)

// motors and planner intimately connected
planner.outputs.X.attach(xMotor.inputs.command)
xMotor.outputs.updates.attach(planner.inputs.X)

planner.outputs.YL.attach(ylMotor.inputs.command)
ylMotor.outputs.updates.attach(planner.inputs.YL)

planner.outputs.YR.attach(yrMotor.inputs.command)
yrMotor.outputs.updates.attach(planner.inputs.YR)

planner.outputs.Z.attach(zMotor.inputs.command)
zMotor.outputs.updates.attach(planner.inputs.Z)