
function writeStateRep(rep, key){
    // ui including different input types ... + button to fire?
    var li = document.createElement('li')
    li.appendChild(document.createTextNode(key + ':'))
    var input = document.createElement('input')
    input.type = 'text'
    input.size = 20
    li.appendChild(input)
    input.addEventListener('change', function(){
        console.log('update', rep.state, key)
    })
    return li
}


function addModule(list, path) {
    // get and add to server system
    if (fs.existsSync(path)) {
        var src = require(path) // get and return 
        var mod = new src() // make a new one
        list.push(mod)
        // assign and id and remember from whence it came
        mod.id = list.length - 1
        mod.path = path

        // we need to add some top-level info to the inputs so that we can draw them 
        for (item in mod.inputs) {
            mod.inputs[item].parentId = mod.id
            mod.inputs[item].key = item
        }

        for (item in mod.state) {
            if (item == 'onChange' | item == 'emitChange' | item == 'emitters') {
                //console.log('rolling past change fn')
            } else {
                mod.state['_' + item] = mod.state[item]
                mod.state[item] = {}
                writeStateObject(mod, item)
            }
        }

        console.log('ADDING MODULE', mod.description.name)

        // now roll and return representable object 
        // first to UI

        // putRep(mod)

        // also to fn call, in case writing program ? 
        return mod
    } else {
        console.log('ERR no module found at', path)
    }
}


/*

fs.writeFile('save/mdls.json', JSON.stringify(modules), 'utf8', function(err){
    if(err) throw err 
    console.log('wrote file')
})

*/

function loadPrgmem(desc) {
    // a collection of modules 
    var modules = new Array()

    // go through once and load in memory 
    for (obj in desc.nodes) {
        console.log(desc.nodes[obj].path)
        addModule(modules, desc.nodes[obj].path)
    }
    for (obj in desc.nodes) {
        var lst = desc.nodes[obj].connects
        // 1st item is output, all others are inputs 
        console.log(lst)
    }
}