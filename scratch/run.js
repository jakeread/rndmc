

const Terminal = require('./src/ui/terminal.js')	
const Gcode = require('./src/parsing/gcode.js')

var terminal = new Terminal()
var gcode = new Gcode()

function writeJson(mod){
	var jswoon = JSON.stringify(mod)
	console.log(jswoon)
}

writeJson(terminal)
writeJson(gcode)

terminal.outputs.lineOut.attach(gcode.inputs.lineIn)
gcode.outputs.instructionOut.attach(terminal.inputs.lineIn)

// think through GET / PUT requests... defaults and particular instances... GET /path instantiates a new one of these, loads it. PUT /path walks to that module, updates relevant features. is this heavy? pointers all the way down

// development environment: 'require' means you can't really re-write on the fly, I think? oy push, recompile and resart? this is kind of what you want - to edit intentionally, saving a new thing, loading a new thing... module would have to save a string copy of itself, load that to browser... just push the file

// server and UI modules: distinct? UI, three.js etc, runs in browser only - events flow up to client... really these can be muxed 

// maybe best is to try the click-to-add runtime, learn about server requests etc, then we'll know enough to start 