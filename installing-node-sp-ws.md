# Installing Node.js, WebSocket and SerialPort | Run atkapi

To run atkapi you'll need to install node.js, and then the packages serialport and ws (websocket).

## Install Node.js

Node.js is a runtime environment for javascript, so you can write and run js locally. [Download and install it here](https://nodejs.org/en/download/). 

To check that node is installed, you can use

``node -v``

In Windows check that in *Environment Variables, System Variables, Path* there is a path for C:\Users\yourusername\npm folder. If the folder does not exist, create it and set the path.  

## Install Serialport

Node comes with a package controller called 'npm' - node package manager. You can use this to install dependencies for node programs. 

Serialport is a package for node that allows it to interact with a hardware serial port.

Navigate to the directory where you'll be running from (atkapi). Do

``npm install serialport``

## Install WS (WebSocket)

WebSockets are very simple web connections. 

To install ws, do

``npm install ws``

## Run atkapi (yay!)

cd to the atkapi folder and run: 

``node main``

You should see this message in the terminal *OPENING THIS PRGRAM REP ...* 

## Open local host

In a browser open *localhost:8080* you will see the mods and this msg in the terminal *SEND PROGRAMS TO UI* 

## Ready to play with atkapi. 