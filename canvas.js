
// business 
const Reps = require('./reps.js')
const Programs = require('./programs.js')

// the program object: real simple, just has a description, and a 'modules' 
var program = Programs.new('new program')

var canvas = Programs.loadModuleFromSource(program, './modules/ui/threeCanvas.js')
Programs.setUI(canvas, 200, 200)

var array = Programs.loadModuleFromSource(program, './modules/util/array.js')
array.state.array = [2,4]
array.outputs.out.attach(canvas.inputs.xy1)

// UI 
const View = require('./views.js')
View.startHttp() 
View.startWs() 

Programs.assignSocket(View.uiSocket)
View.assignProgram(program)