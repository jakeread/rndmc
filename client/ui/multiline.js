(function() {
    /* KRITICAL STEP */
    // this is our object, that we will later append
    // to the .lump position 
    var multiline = {}

    // our dom element
    var li = document.createElement('li')
    var label = document.createTextNode('temp label')
    li.appendChild(label)
    li.appendChild(document.createElement('br'))
    var txtArea = document.createElement('textarea')
    txtArea.rows = 25
    txtArea.cols = 45
    txtArea.value = '-'
    txtArea.addEventListener('change', function() {
        var data = {
            id: multiline.parentId,
            key: multiline.key,
            msg: txtArea.value 
        }
        socketSend('put ui change', data)
    })
    li.appendChild(txtArea)

    /* KRITICAL STEP */
    // we have to give our 'lump' object a .domElem, this
    // will be referenced upstream to append to the right module 
    // append it to the dom so that it can be appended on init 
    multiline.domElem = li

    /* KRITICAL STEP */
    // whatever is posted to .lump will receive messages through
    // .onMessage, so if we don't write one, we'll cause errors
    // upstream, and besides, wouldn't be able to get anything from
    // the server 
    multiline.onMessage = function(msg) {
        //console.log('got message in client side ui object', msg)
        if (msg.call == 'setContents') {
            txtArea.value = msg.argument
        } else if (msg.call == 'setLabel'){
        	label.textContent = msg.argument 
        } else if (msg.call == 'setRows') {
        	txtArea.rows = msg.argument 
        }
    }

    /* KRITICAL STEP */
    // expect this to only be used once
    // it's basically our init function 
    // and gets called once the module is loaded 
    window.registerNewModule = function(id, key) {
        multiline.parentId = id
        multiline.key = key
        // affectionately named lump of code, insert ourselves here 
        program.modules[id].ui[key].lump = multiline
        // and call-back to do onload things
        var data = {
            id: multiline.parentId,
            key: multiline.key,
            msg: 'onload'
        }
        socketSend('put ui change', data)
    }
})()