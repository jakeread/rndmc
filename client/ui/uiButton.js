(function(){
	/* KRITICAL STEP */
	// this is our object, that we will later append
	// to the .lump position 
	var btn = {}

	// our dom element
	var li = document.createElement('li')
	li.innerHTML = 'button test'
	li.addEventListener('click', function(evt){
		// example of how to send data back to server 
		var data = {
			id: btn.parentId,
			key: btn.key,
			msg: {
				key: 'onclick'
			}
		}
		socketSend('put ui change', data)
	})

	/* KRITICAL STEP */
	// we have to give our 'lump' object a .domElem, this
	// will be referenced upstream to append to the right module 
	// append it to the dom so that it can be appended on init 
	btn.domElem = li 

	/* KRITICAL STEP */
	// whatever is posted to .lump will receive messages through
	// .onMessage, so if we don't write one, we'll cause errors
	// upstream, and besides, wouldn't be able to get anything from
	// the server 
	btn.onMessage = function(msg){
		if(msg.calls == 'setText'){
			btn.domElem.innerHTML = msg.argument
		}
	}

	/* KRITICAL STEP */
	// expect this to only be used once
	// it's basically our init function 
	// and gets called once the module is loaded 
	window.registerNewModule = function(id, key){
		btn.parentId = id 
		btn.key = key 
		// affectionately named lump of code, insert ourselves here 
		program.modules[id].ui[key].lump = btn 
		// and call-back to do onload things
		var data = {
			id: btn.parentId,
			key: btn.key,
			msg: {
				key: 'onload'
			}
		}
		socketSend('put ui change', data)
	}
})()