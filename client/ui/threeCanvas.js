(function() {
    // generic / example three.js canvas 
    /* KRITICAL STEP */
    // this is our object, that we will later append
    // to the .lump position 
    var threeCanvas = {}

    // now three stuff 
    var container = document.createElement('div')
    var scene = new THREE.Scene()
    scene.background = new THREE.Color(0xd6d6d6)
    var camera = new THREE.PerspectiveCamera(75, 1, 0.1, 1000)
    camera.up.set(0, 0, 1)
    var renderer = new THREE.WebGLRenderer()
    renderer.setSize(396, 396)
    container.appendChild(renderer.domElement)

    // axes 
    var axesHelper = new THREE.AxesHelper(0.1)
    scene.add(axesHelper)

    // grid 
    /*
    var gridHelper = new THREE.GridHelper(10, 100)
    gridHelper.translateOnAxis(new THREE.Vector3(0, 0, 1), -0.01)
    gridHelper.rotateX(-Math.PI / 2)
    scene.add(gridHelper)
    */

    // one line 
    var material = new THREE.LineBasicMaterial({ color: 0x000000 })
    var geometry = new THREE.Geometry()
    geometry.vertices.push(new THREE.Vector3(0, 0, 0))
    geometry.vertices.push(new THREE.Vector3(0.5, 0, 0.5))
    geometry.vertices.push(new THREE.Vector3(1, 0, 0.5))
    var line = new THREE.Line(geometry, material)
    scene.add(line)

    var controls = new THREE.OrbitControls(camera, container)

    camera.position.set(1, -2, 2)
    controls.update()

    console.log(line.geometry)

    var animate = function() {
        requestAnimationFrame(animate)
        /*
        line.geometry.vertices[1].x += 0.01
        line.geometry.verticesNeedUpdate = true
        */
        controls.update()

        renderer.render(scene, camera)
    }
    // kickstart 
    animate()


    // set class, etc, put canvas in class, etc 
    // and then 
    /* KRITICAL STEP */
    // we have to give our 'lump' object a .domElem, this
    // will be referenced upstream to append to the right module 
    // append it to the dom so that it can be appended on init 
    threeCanvas.domElem = container

    /* KRITICAL STEP */
    // whatever is posted to .lump will receive messages through
    // .onMessage, so if we don't write one, we'll cause errors
    // upstream, and besides, wouldn't be able to get anything from
    // the server 
    threeCanvas.onMessage = function(msg) {
        //console.log('got message in client side ui object', msg)
        if (msg.call == 'setXY1') {
            line.geometry.vertices[1].set(msg.argument[0], msg.argument[1], msg.argument[2])
            line.geometry.verticesNeedUpdate = true 
        } else if (msg.call == 'setXY2') {
            line.geometry.vertices[2].set(msg.argument[0], msg.argument[1], msg.argument[2])
            line.geometry.verticesNeedUpdate = true 
        } else if (msg.call == 'setRows') {
            //  
        } else {
            console.log('default msg because none from sys at threejs clientside')
        }
    }

    /* KRITICAL STEP */
    // expect this to only be used once
    // it's basically our init function 
    // and gets called once the module is loaded 
    window.registerNewModule = function(id, key) {
        threeCanvas.parentId = id
        threeCanvas.key = key
        // affectionately named lump of code, insert ourselves here 
        program.modules[id].ui[key].lump = threeCanvas
        // and call-back to do onload things
        var data = {
            id: threeCanvas.parentId,
            key: threeCanvas.key,
            msg: {
                key: 'onload'
            }
        }
        socketSend('put ui change', data)
    }
})()