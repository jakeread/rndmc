function distance(p1, p2) {
    // takes p1, p2 to be arrays of same length
    // computes cartesian distance
    var sum = 0
    for (var i = 0; i < p1.length; i++) {
        sum += Math.pow((p1[i] - p2[i]), 2)
    }
    return Math.sqrt(sum)
}

function length(v) {
    // length of vector
    var sum = 0
    for (var i = 0; i < v.length; i++) {
        sum += Math.pow(v[i], 2)
    }
    return Math.sqrt(sum)
}

function degrees(rad){
    return rad * (180 / Math.PI)
}

module.exports = {
    distance: distance,
    length: length,
    degrees: degrees
}