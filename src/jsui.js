function UI() {
    var ui = {}

    ui.addElement = function(keyName, clientPath){
        ui[keyName] = UIElement(keyName, clientPath, this)
    }

    ui.init = function(parentModId, socket){
    	// get hookups from top level program 
        this.parentId = parentModId
        this.socket = socket
    }

    return ui
}

function UIElement(keyName, clientPath, ui){
    var element = {
        clientPath: clientPath,
        calls: {}
    }

    element.call = function(functionName, argument){
        this.send({
            calls: functionName,
            argument: argument
        })
    }

    element.send = function(msg){
        var data = {
            id: ui.parentId,
            key: keyName, 
            msg: msg
        }
        ui.socket.send('put ui change', data)
    }

    element.subscribe = function(msgKey, callback){
        this.calls[msgKey] = callback
    }

    element.onMessage = function(msg){
        if(this.calls[msg.key] != null){
            this.calls[msg.key](msg)
        } else {
            console.log('UI Element from ID', ui.parentId, 'calls for msg having no callback:', msg)
        }
    }

    return element
}

function isUiKey(key) {
    if (key == 'parentId' || key == 'socket' || key == 'init' || key == 'addElement' || key == 'pushToUI' || key == 'emitters') {
        return false
    } else {
        return true
    }
}

module.exports = {
	UI: UI,
    UIElement: UIElement,
	isUiKey: isUiKey
}

/* bless u mdn 
https://developer.mozilla.org/en-US/docs/Web/API/HTMLScriptElement
https://developers.google.com/web/fundamentals/primers/modules
*/