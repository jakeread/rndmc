// software object for reciprocal hardware object 

// boilerplate atkapi header
const JSUnit = require('./jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State
let Button = JSUnit.Button

// interface elements
const JSUI = require('./jsui.js')
let UI = JSUI.UI

let ATKRoute = require('./atkroute.js')

function Hardware() {
    var hardware = {
        description: {
            name: 'hardwareUnit',
            alt: 'software representation of networked hardware object',
            isHardware: true
        },
        route: ATKRoute('0,0')
    }

    hardware.state = State()
    var state = hardware.state

    state.message = 'click below to test network'
    state.route = '0,0' // default  

    hardware.ui = UI()
    var ui = hardware.ui

    ui.addElement('resetButton', 'ui/uiButton.js')
    ui.resetButton.subscribe('onload', function(msg) {
        ui.resetButton.send({
            calls: 'setText',
            argument: 'reset hardware'
        })
    })
    ui.resetButton.subscribe('onclick', onReset)

    ui.addElement('testButton', 'ui/uiButton.js')
    ui.testButton.subscribe('onload', function(msg) {
        ui.testButton.send({
            calls: 'setText',
            argument: 'test network'
        })
    })
    ui.testButton.subscribe('onclick', onNetworkTest)

    state.onUiChange('route', function() {
        console.log('logging state / route change')
        hardware.route.route = state.route
    })

    function onReset() {
        var rstpck = new Array()
        rstpck.push(128)
        state.message = 'reset command issued'
        hardware.route.send(rstpck)
    }

    function onNetworkTest() {
        var tstpck = new Array()
        tstpck.push(127)
        state.message = 'test packet out'
        hardware.route.send(tstpck)
    }

    hardware.route.subscribe(127, testReturn)

    function testReturn(msg) {
        state.message = 'test OK'
        console.log('test returns with msg', msg)
    }

    return hardware
}

module.exports = Hardware