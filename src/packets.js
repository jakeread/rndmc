function pack32(val) {
    var pack = new Array();
    pack[0] = (val >> 24) & 255;
    pack[1] = (val >> 16) & 255;
    pack[2] = (val >> 8) & 255;
    pack[3] = val & 255;

    return pack;
}

function unPack32(arr){
	if(arr.length == 4){
		var unPacked = arr[0] << 24 | arr[1] << 16 | arr[2] << 8 | arr[3] 
		return unPacked
	} else {
		console.log("ERR: arr > 4 at unPack32", arr)
	}
}

function packFloatTo32(val, debug){
	// https://www.h-schmidt.net/FloatConverter/IEEE754.html
	var f = new Float32Array(1) 
	f[0] = val 
	var view = new Uint8Array(f.buffer)
	if(debug) printViewBytes(view)
	var pack = new Array()
	pack[0] = view[3]
	pack[1] = view[2]
	pack[2] = view[1]
	pack[3] = view[0]
	return pack 
}

function printViewBytes(view){
	for(i = view.length - 1; i >= 0; i --){
		var bits = view[i].toString(2) 
		if(bits.length < 8){
			bits = new Array(8 - bits.length).fill('0').join('') + bits
		}
		console.log(bits)
	}
}

module.exports = {
	pack32: pack32,
	unPack32: unPack32,
	packFloatTo32: packFloatTo32
}