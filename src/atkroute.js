// object to extend for things-that-are-hardware

// and pass f'n to call on received messages
function ATKRoute(route, calls) {
    var atkroute = {
        isAtkRoute: true,
        link: null, // pls, deliver 2 me a hw outlet 
        route: route,
        calls: {}
    }

    atkroute.send = function(msg) {
        if (this.link != null) {
            // CHECKS and append route, then send 
            this.link.send(msg, this)
        } else {
            console.log("NO LINK NO SEND")
        }
    }

    atkroute.subscribe = function(key, callback){
        this.calls[key.toString()] = callback
    }

    atkroute.onMessage = function(msg){
        // one key at a time, for now
        var key = msg[0].toString()
        // *could* slice the key out at this point, but nah 
        if(this.calls[key] != null){
            this.calls[key](msg)
        }
    }

    return atkroute
}

module.exports = ATKRoute