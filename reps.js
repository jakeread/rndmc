const JSUnit = require('./src/jsunit.js')
let isStateKey = JSUnit.isStateKey

const JSUI = require('./src/jsui.js')
let isUiKey = JSUI.isUiKey

function makeRepFromModule(mdl) {
    // rep != mdl 
    // rep is rep of mdl 

    // deep copy description 
    var rep = {
        description: JSON.parse(JSON.stringify(mdl.description))
    }

    // TODO: making rep. of input / output should be a f'n of that object ...
    // input, outputs, state objs should be known /sysobjects 
    // everything else is free play 
    rep.inputs = {}
    for (key in mdl.inputs) {
        rep.inputs[key] = {}
        rep.inputs[key].accepts = mdl.inputs[key].accepts
    }

    rep.outputs = {}
    for (key in mdl.outputs) {
        rep.outputs[key] = {}
        rep.outputs[key].emits = mdl.outputs[key].emits
        rep.outputs[key].calls = new Array()
        mdl.outputs[key].calls.forEach(function(inp) {
            var input = {
                parentId: inp.parentId,
                key: inp.key
            }
            rep.outputs[key].calls.push(input)
        })
    }

    rep.state = {}
    for(key in mdl.state){
        if(isStateKey(key)){
            rep.state[key] = mdl.state[key]
        }
    }

    rep.ui = {}
    for(key in mdl.ui){
        if(isUiKey(key)){
            rep.ui[key] = {} 
            rep.ui[key].type = mdl.ui[key].type 
            rep.ui[key].clientPath = mdl.ui[key].clientPath  
            if(mdl.ui[key].libPath != null){
                rep.ui[key].libPath = mdl.ui[key].libPath  
            }
        }
    }

    return rep
}

module.exports = {
    makeFromModule: makeRepFromModule
}