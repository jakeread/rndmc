// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')

let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// websocket, to share program representations with the client (and back)
const ws = require('ws')

function WebSocket() {
    var webSocket = {
        description: {
            name: 'webSocket',
            alt: 'webseket'
        }
    }

    // log more 
    var verbose = false 

    let sckt = null 

    // one caveat here is that we can't dynamically add objects to this, 
    // or they don't get getter / settered when we do
    webSocket.state = State()
    var state = webSocket.state
    state.address = '127.0.0.1'
    state.port = 1234
    state.status = 'closed'

    webSocket.inputs = {
        in: Input('any', (thing) => {
            // some fn 
        })
    }

    webSocket.outputs = {
        data: Output('any')
    }

    function openSocket(){
        const wss = new ws.Server({port: state.port})
        wss.on('connection', (pipe) => {
            sckt = pipe 
            console.log('WebSocket Opened')
            sckt.on('message', (msg) => {
                webSocket.outputs.data.emit(JSON.parse(msg))
            })
        })
    }

    openSocket() 

    return webSocket
}


// export the module 
module.exports = WebSocket