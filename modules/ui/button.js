// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI 

// a constructor, a fn, a javascript mess
function uiButton() {

    var button = {
        // descriptions are used in UI
        description: {
            name: 'Button',
            alt: 'for clicking'
        }
    }

    button.state = State()
    // alias !
    var state = button.state 

    button.ui = UI() 
    var ui = button.ui 
    ui.addElement('btn', 'ui/uiButton.js')
    ui.btn.subscribe('onload', function(msg){
        ui.btn.send({
            calls: 'setText',
            argument: 'click!'
        })
    })
    ui.btn.subscribe('onclick', onButtonPress)

    button.inputs = {
        thru: Input('any', onButtonPress) // makes anything into '1' event
    }

    button.outputs = {
        whammy: Output('number')
    }

    function onButtonPress(evt){
        button.outputs.whammy.emit(1)
    }

    return button
}

// exports 
module.exports = uiButton