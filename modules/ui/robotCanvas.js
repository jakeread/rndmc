// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI

// a constructor, a fn, a javascript mess
function RobotCanvas() {

    var rbtCanvas = {
        // descriptions are used in UI
        description: {
            name: 'RobotArm-Canvas',
            alt: 'graphix',
            isBigBlock: true 
        }
    }

    rbtCanvas.state = State()
    // alias !
    var state = rbtCanvas.state

    rbtCanvas.inputs = {
        xy1: Input('array', onNewXY1),
        xy2: Input('array', onNewXY2),
        xy3: Input('array', onNewXY3)
    }

    rbtCanvas.outputs = {
        //log: Output('string')
    }

    rbtCanvas.ui = UI()
    var ui = rbtCanvas.ui
    ui.addElement('canvas', 'ui/robotRepresentationTubes.js')
    // add bonus lib path 
    ui.canvas.libPath = 'ui/libs/three.js'
    ui.canvas.subscribe('onload', function(msg){
        console.log('catch canvas load', msg)
        onNewXY1([0,0,0])
        onNewXY2([0.3,0.3,0.6])
        onNewXY3([0.6,0.6,0.3])
    })
    
    function onNewXY1(array) {
        ui.canvas.send({
            calls: "updateXY1",
            argument: array
        })
    }

    function onNewXY2(array){
        ui.canvas.send({
            calls: "updateXY2",
            argument: array
        })
    }

    function onNewXY3(array){
        ui.canvas.send({
            calls: "updateXY3",
            argument: array
        })
    }

    return rbtCanvas
}

// exports 
module.exports = RobotCanvas