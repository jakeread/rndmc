// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI

/* WARN
not refactored since UI API refactor
*/

const fs = require('fs')

// a constructor, a fn, a javascript mess
function MultiLineIn() {

    console.log("WARNING - LOADING MULTILINE - not refactored since UI API refactor")

    var multilinein = {
        // descriptions are used in UI
        description: {
            name: 'Multi-line Text Input',
            alt: 'sequential txt input'
        }
    }

    multilinein.state = State()
    // alias !
    var state = multilinein.state

    multilinein.ui = UI()
    var ui = multilinein.ui
    ui.addElement('thruButton', 'ui/uiButton.js')
    ui.thruButton.subscribe('onload', function(msg) {
        ui.thruButton.send({
            calls: 'setText',
            argument: 'click to advance line-by-line'
        })
    })
    ui.thruButton.subscribe('onclick', lineThru)

    ui.addElement('previously', './ui/multiline.js')
    ui.previously.subscribe('onload', function(msg) {
        ui.previously.send({
            calls: 'setContents',
            argument: '-'
        })
        ui.previously.send({
            calls: 'setLabel',
            argument: 'previously:'
        })
        ui.previously.send({
            calls: 'setRows',
            argument: 15
        })
    })

    ui.addElement('justNow', './ui/multiline.js')
    ui.justNow.subscribe('onload', function(msg) {
        ui.justNow.send({
            calls: 'setContents',
            argument: '-'
        })
        ui.justNow.send({
            calls: 'setlabel',
            argument: 'just now:'
        })
        ui.justNow.send({
            calls: 'setRows',
            argument: 1
        })
    })

    ui.addElement('incoming', './ui/multiline.js')
    ui.incoming.subscribe('onload', function(msg) {
        //ui.incoming.setContents('G0 F50 X10Y10Z10\nG0 X20Y20Z0\nG0 X0\nG0 Y10\nG0 F50 X10Y10Z10\nG0 X20Y20Z0\nG0 X0\nG0 Y10\nG0 F50 X10Y10Z10\nG0 X20Y20Z0\nG0 X0\nG0 Y10\nG0 F50 X10Y10Z10\nG0 X20Y20Z0\nG0 X0\nG0 Y10\n')
        ui.incoming.send({
            calls: 'setLabel',
            argument: 'incoming:'
        })
        ui.incoming.send({
            calls: 'setRows',
            argument: 50
        }) 
        onLoadFile('./files/dogbone.gcode')
    })

    multilinein.inputs = {
        req: Input('number', onLineRequest),
        lineIn: Input('string', onExternalLine),
        load: Input('path', onLoadFile)
    }

    multilinein.outputs = {
        lineOut: Output('string')
    }

    // internal funcs?

    function lineThru() {
        // get all as arrays with linebreak as delimiter 

        var outBox = ui.previously.contents.split('\n')
        var box = ui.justNow.contents
        var inBox = ui.incoming.contents.split('\n')

        if (inBox.length < 1 && box == '') {
            console.log("END OF MULTILINE")
        } else {
            outBox.push(box)
            ui.justNow.setContents(checkContent(inBox.shift()))
            ui.previously.setContents(checkContent(outBox.join('\n')))
            ui.incoming.setContents(checkContent(inBox.join('\n')))
            if (inBox.length < 1 && box == '') {
                console.log("END OF MULTILINE")
            } else {
                multilinein.outputs.lineOut.emit(box)
            }
        }
    }

    function checkContent(string) {
        if (typeof string == 'string') {
            return string
        } else {
            return '-'
        }
    }

    function onLineRequest(num) {
        console.log('-------------------------- ON LINE REQ', num)
        for (var i = 0; i < num; i++) {
            lineThru()
        }
    }

    function onLoadFile(path) {
        fs.readFile(path, 'utf8', (err, data) => {
            if (err) throw err;
            console.log('Loading:')
            console.log(data);
            ui.incoming.send({
                calls: 'setContents',
                argument: data
            })
        })
    }

    multilinein.load = onLoadFile

    function onExternalLine(str) {
        // push new str to bottom of queue
    }

    return multilinein
}

// exports 
module.exports = MultiLineIn