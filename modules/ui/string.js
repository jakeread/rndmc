// boilerplate rndmc header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI 

// a constructor, a fn, a javascript mess
function uiString() {

    // this is the tiny program-as-and-object that we'll load into rundmc 
    // description / name is required to load successfully 
    var uistring = {
        description: {
            name: 'string-output',
            alt: 'for clicking'
        }
    }

    // the State() object is what the system scrapes for ui variables / updates from the UI
    // this includes things like Button('title', callback), which are unique state variables
    // they can also be found in jsunit.js 
    uistring.state = State()
    // alias !
    var state = uistring.state 

    state.string = 'something'

    uistring.ui = UI() 
    var ui = uistring.ui 
    ui.addElement('onNumberButton', 'ui/uiButton.js')
    ui.onNumberButton.subscribe('onload', function(msg){
        ui.onNumberButton.send({
            calls: 'setText',
            argument: 'number out ->'
        })
    })
    ui.onNumberButton.subscribe('onclick', onStringDesire)

    // inputs are required, and must be Input('type', callback) 
    uistring.inputs = {
        thru: Input('any', onThruInput), // makes anything into string event 
        evt: Input('any', onStringDesire)
    }

    // outputs: Output('type')
    uistring.outputs = {
        out: Output('string')
    }

    // here's our input callback, specified in the input constructor 
    function onThruInput(input){
        if(typeof input == 'string'){
            state.string = input
        } else {
            state.string = input.toString()
        }
        onStringDesire()
    }

    function onStringDesire(){
        // here's how we fire an output. 
        uistring.outputs.out.emit(state.string)
    }

    // gotta give the program this thing we made 
    return uistring
}

// this for node.js's require() function 
module.exports = uiString