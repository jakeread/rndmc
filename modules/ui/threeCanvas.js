// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI

// a constructor, a fn, a javascript mess
function PointOnCanvas() {

    var ptOnCanvas = {
        // descriptions are used in UI
        description: {
            name: 'ThreeJS-Canvas',
            alt: 'graphix'
        }
    }

    ptOnCanvas.state = State()
    // alias !
    var state = ptOnCanvas.state

    ptOnCanvas.inputs = {
        xy1: Input('array', onNewXY1),
        xy2: Input('array', onNewXY2)
        // do some canvas stuff 
    }

    ptOnCanvas.outputs = {
        //log: Output('string')
    }

    ptOnCanvas.ui = UI()
    var ui = ptOnCanvas.ui
    ui.addElement('threeCanvas', './ui/threeCanvas.js', onUICallback)
    ui.threeCanvas.onload = function() {
        console.log('canvas is loaded')
    }

    function onNewXY1(array) {
        ui.threeCanvas.updateXY1([array[0], 0, array[1]])
    }

    function onNewXY2(array){
        ui.threeCanvas.updateXY2([array[0], 0, array[1]])
    }

    function onUICallback(msg) {
        console.log('msg callback from threeCanvas ui element', msg)
    }

    return ptOnCanvas
}

// exports 
module.exports = PointOnCanvas