// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// a constructor, a fn, a javascript mess
function Terminal() {

    var terminal = {
        // descriptions are used in UI
        description: {
            name: 'Terminal',
            alt: 'txt input'
        }
    }

    terminal.state = State()
    terminal.state.uiInput = 'line in here !'

    terminal.inputs = {
        lineIn: Input('string', newInput)
    }

    terminal.outputs = {
        lineOut: Output('string')
    }

    // we'll occasionally want a hook here to do stuff when a state variable is changed
    // these will fire if we change them internally, or if they're changed from the ui
    
    terminal.state.onUiChange('uiInput', function(){
        terminal.outputs.lineOut.emit(terminal.state.uiInput)
    })
    
    // internal funcs?

    function liChange(value){
        //
    }

    function newInput(str){
        // 
    }

    return terminal
}

// exports 
module.exports = Terminal