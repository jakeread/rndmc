const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI 

function STest() {
	var stest = {
		description:{
			name: 'test-for-state',
			alt: 'a description'
		}
	}

	// state is numbers, strings, arrays of either, booleans
	// booleans are buttons 
	stest.state = State() 
	var state = stest.state 
	state.num = 12 
	state.str = 'one string'

	state.arr = new Array()
	state.arr.push(12)
	state.arr.push(24)

	state.strarr = new Array()
	state.strarr.push('str1')
	state.strarr.push('str2')

	state.boolean = false 

	stest.inputs = {
		inp: Input('any', onInp)
	}

	function onInp(evt){
		console.log('ex input', evt)
	}

	stest.outputs = {
		outp: Output('any')
	}


	// other items for interaction are explicitly UI 
	stest.ui = UI() 
	var ui = stest.ui 
	ui.addElement('btnex', 'ui/uiButton.js')
	ui.btnex.subscribe('onload', function(msg){
		console.log('ok, yes, loaded')
    })
    ui.btnex.subscribe('onclick', onButtonData)

	function onButtonData(evt){
		console.log('on module callback', evt)
		ui.btnex.send({
            calls: 'setText',
            argument: 'hello from stest'
        })
	}

	return stest
}

module.exports = STest 