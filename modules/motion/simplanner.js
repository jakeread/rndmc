// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI

// descartes, to you 
const DCRT = require('../../src/cartesian.js')
const MJS = require('mathjs')

// planner consumes target moves (i.e. segments having uniform speed throughout)
// and subjects them to acceleration constraints 

function Planner() {
    var planner = {
        description: {
            name: 'Lookahead-Motion-Planner',
            alt: 'movements -> acceleration planned moves'
        }
    }

    // log more 
    var verbose = false

    planner.state = State()
    var state = planner.state // reference pass attempt?

    state.axisIDs = 'X,Y,Z'

    state.accel = 200 // units/s/s
    state.minSpeed = 1 // units/s

    state.position = [0, 0, 0]

    state.isRunning = 1

    state.netWindow = 1
    state.netState = 0

    planner.ui = UI()
    var ui = planner.ui

    planner.inputs = {
        instruction: Input('move instruction', onNewInstruction),
        acks: Input('move acknowledgement', onAck),
        positions: Input('new positions', onPositions)
    }

    planner.outputs = {
        moves: Output('move instruction'),
        moveComplete: Output('number')
    }

    /*
    ------------------------------------------------------
    UPDATING / SETUP
    ------------------------------------------------------
    */

    function onPositions(msg){
        //console.log("Planner onAck:", msg)
        // update position, net states, run netCheck
        // positions ... 
        var axes = state.axisIDs.split(',')
        var match = axes.indexOf(msg.axis)
        if (match !== -1) {
            // HERE is a hack, for our state-updating system doesn't yet accomodate arrays 
            var newPos = state.position.slice(0)
            newPos[match] += msg.increment
            state.position = newPos
        } else {
            console.log('ERR in PLANNER: missed axis on ack from stepper')
        }
    }

    function onAck(msg) {
        // just count to four 
        state.netState ++
        if(state.netState >= 4){
            console.log('Planner confirms Move Complete')
            state.netState = 0
            planner.outputs.moveComplete.emit(1)
        }
        
    }

    function sendMoveToNetwork(move) {
        planner.outputs.moves.emit(move)
    }

    /*
    ------------------------------------------------------
    ENTRY POINTS
    ------------------------------------------------------
    */

    function onNewInstruction(move) {
        // our axis
        var axes = state.axisIDs.split(',')

        // we'll make a new move object
        // start and end points, and axes to track
        var p1 = []
        var p2 = new Array(axes.length)
        p2.fill(0)
        // start point is current pos ... one at a time in simplanner 
        p1 = state.position
        // now pick out new deltas
        for (i in axes) {
            var key = axes[i]
            if (move.position[key] != null) {
                // some new pos,
                p2[i] = move.position[key]
            } else {
                // or none, this axis stays 
                p2[i] = p1[i]
            }
        }
        // check for zero-length vector 
        if (MJS.distance(p1, p2) == 0) {
            console.log('------------------ !ACHTUNG! -------------------')
            console.log('------------------ !ACHTUNG! -------------------')
            console.log('planner throwing zero length vector')
            // this means we need another one from the queue
            planner.outputs.moveComplete.emit(1)
        } else {
            // starting with basics
            let mv = {
                axes: axes,
                p1: p1,
                p2: p2,
                cruise: move.speed,
                entry: state.minSpeed,
                exit: state.minSpeed,
                accel: state.accel
            }
            // run it once 
            runSimpleAccel(mv)
            sendMoveToNetwork(mv)
        }
    }

    /*
    ------------------------------------------------------
    JUNCTION DEVIATION 
    walk mq to generate permissible entry / exit speeds 
    by 'junction deviation' algorithm 
    ------------------------------------------------------
    */

    function runSimpleAccel(move) {
        // useful
        var accel = state.accel
        var ms = state.minSpeed
        // should reference axis ids, not these moves ? 
        var numAxis = move.p1.length

        // have entry and exit speeds already because we're simplanner
        // now that we know permissible cornering speeds,
        // we'll calculate the trapezoid shapes
        // this becomes very useful for turning moves over to stepper motors
        // and will give us a time estimate for each move as well,
        // we need / want that so that we can set a network buffer length 
        calcTrap(move, accel, false)
        let time = moveTime(move)
        if (true) console.log('PLANNER: trapezoid time to', move.p2, time)
        if (time < 0.1) {
            console.log('------------------ !ACHTUNG! -------------------')
            console.log('------------------ !ACHTUNG! -------------------')
            console.log("WARN! move time here", time)
        }
    }
    /*
    ------------------------------------------------------
    TRAPEZOIDS FROM JUNCTION DEVIATION ENTRY / EXITS
    ------------------------------------------------------
    */

    function calcTrap(move, a, debug) {
        // have p1, p2, cruise, entry, exit
        // add length, figure if full-accel, full-deccel, triangle, trapezoid, cruise ...
        move.vector = MJS.subtract(move.p2, move.p1)
        var d = DCRT.length(move.vector)
        var vi = move.entry
        var v = move.cruise
        var vf = move.exit
        // limits
        var maxExit = Math.sqrt(Math.pow(vi, 2) + 2 * a * d)
        var maxEntry = Math.sqrt(Math.pow(vf, 2) + 2 * a * d)
        // seven possible cases
        // HERE set move with type, accel / deccel lengths, calculate time for each, and total time 
        // then get on with writing a larger gcode window, bringing in some 'real' moves, and getting these
        // out / back from steppers 
        if (d <= 0) {
            console.log('ZERO LENGTH MOVE')
        } else if (maxExit <= vf) {
            if (debug) console.log('TRAP: full ramp up');
            move.type = 'ramp up'
            // full ramp up
            // accel, cruise, and deccel time 
            move.t1 = (vf - vi) / a
            move.d1 = d
            move.t2 = 0
            move.d2 = 0
            move.t3 = 0
            move.d3 = 0
            // check we won't break
            if (maxExit < vf) console.log('max ramp up, misses by', vf - maxExit)
        } else if (maxEntry <= vi) {
            if (debug) console.log('TRAP: full ramp down');
            move.type = 'ramp down'
            move.t1 = 0
            move.d1 = 0
            move.t2 = 0
            move.d2 = 0
            move.t3 = (vi - vf) / a
            move.d3 = d
            // check we're not going to break / not make
            if (maxEntry < vi) console.log('max ramp down, misses by', vi - maxEntry)
        } else if (vi == v && vf == v) {
            if (debug) console.log('TRAP: full cruise');
            move.type = 'cruise'
            move.t1 = 0
            move.d1 = 0
            move.t2 = d / v
            move.d2 = d
            move.t3 = 0
            move.d3 = 0
        } else if (vi == v) {
            if (debug) console.log('TRAP: cruise, decelerate');
            move.type = 'cruise and deccel'
            var deccelDistance = (Math.pow(v, 2) - Math.pow(vf, 2)) / (2 * a)
            move.t1 = 0
            move.d1 = 0
            move.t2 = (d - deccelDistance) / v
            move.d2 = d - deccelDistance
            move.t3 = (v - vf) / a
            move.d3 = deccelDistance
        } else if (vf == v) {
            if (debug) console.log('TRAP: accel, cruise');
            move.type = 'accel and cruise'
            var accelDistance = (Math.pow(v, 2) - Math.pow(vi, 2)) / (2 * a)
            move.t1 = (v - vi) / a
            move.d1 = accelDistance
            move.t2 = (d - accelDistance) / v
            move.d2 = d - accelDistance
            move.t3 = 0
            move.d3 = 0
        } else {
            // either full trapezoid, or a triangle 
            // distances to / from cruise from / to start / end 
            var accelDistance = (Math.pow(v, 2) - Math.pow(vi, 2)) / (2 * a)
            var deccelDistance = (Math.pow(v, 2) - Math.pow(vf, 2)) / (2 * a)
            if (accelDistance + deccelDistance < d) {
                if (debug) console.log('TRAP: complete trapezoid');
                move.type = 'complete trapezoid'
                move.t1 = (v - vi) / a
                move.d1 = accelDistance
                move.t2 = (d - accelDistance - deccelDistance) / v
                move.d2 = d - accelDistance - deccelDistance
                move.t3 = (v - vf) / a
                move.d3 = deccelDistance
            } else {
                if (debug) console.log('TRAP: triangular');
                // find the peak 
                var vPeak = Math.sqrt(((2 * a * d + Math.pow(vi, 2) + Math.pow(vf, 2)) / 2))
                var midPoint = (Math.pow(vPeak, 2) - Math.pow(vi, 2)) / (2 * a)
                move.type = 'triangular'
                move.t1 = (v - vi) / a
                move.d1 = midPoint
                move.t2 = 0
                move.d2 = 0
                move.t3 = (v - vf) / a
                move.d3 = d - midPoint
            }
        }
    }

    function moveTime(move) {
        if (move.t1 != null) {
            return move.t1 + move.t2 + move.t3
        } else {
            console.log('move with no time')
        }
    }

    return planner
}

module.exports = Planner