// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')

let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI

// unit to convert neil's mods-output arrays into human readable move objects 
// probably do flow control here also ?

function UserMove() {

    var userMove = {
        description: {
            name: 'userMove gen',
            alt: 'line of userMove -> points'
        }
    }

    // one caveat here is that we can't dynamically add objects to this, 
    // or they don't get getter / settered when we do
    userMove.state = State()
    var state = userMove.state
    state.moveTo = '100,100,10'
    state.moveSpeed = 100

    userMove.inputs = {
        go: Input('event', (evt) => {
            sendMove()
        })
    }

    userMove.outputs = {
        move: Output('move')
    }

    userMove.ui = UI()

    var ui = userMove.ui
    ui.addElement('goButton', 'ui/uiButton.js')
    ui.goButton.subscribe('onload', function(msg) {
        ui.goButton.send({
            calls: 'setText',
            argument: 'click to send move'
        })
    })
    ui.goButton.subscribe('onclick', (evt) => {
        sendMove()
    })

    function sendMove() {
        var axes = state.moveTo.split(',')
        if (axes.length == 3) {
            let move = {
                position: {
                    X: parseFloat(axes[0]),
                    Y: parseFloat(axes[1]),
                    Z: parseFloat(axes[2]),
                },
                speed: state.moveSpeed
            }
            console.log('User Move is', move)
            userMove.outputs.move.emit(move)
        } else {
            console.log("len for moves must be three")
        }

    }

    return userMove
}

// export the module 
module.exports = UserMove