// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')

let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// unit to convert neil's mods-output arrays into human readable move objects 
// probably do flow control here also ?

function NeilParser() {
    var neilParser = {
        description: {
            name: 'neilParser Parser',
            alt: 'line of neilParser -> points'
        }
    }

    // log more 
    var verbose = false

    let npList = null

    // one caveat here is that we can't dynamically add objects to this, 
    // or they don't get getter / settered when we do
    neilParser.state = State()
    var state = neilParser.state
    state.unitsPerMM = 8
    state.lift = 5
    state.moveSpeed = 100
    state.plungeSpeed = 20
    state.jogSpeed = 200
    state.listLength = 0

    neilParser.inputs = {
        array: Input('array', (arr) => {
            let list = writeMoveObjects(arr)
            console.log('have list', list)
            npList = list
            state.listLength = npList.length
            // throw it out 
            neilParser.outputs.newList.emit(1)
            onShift() 

        }),
        shift: Input('event', (arg) => {
            onShift() 
        })
    }

    neilParser.outputs = {
        move: Output('move'),
        newList: Output('event')
    }

    function writeMoveObjects(arr) {
        // arr[i] are segments, 
        // between arr[i] instances, we put a z-lift and then 
        // a z-drop 

        if (Array.isArray(arr)) {
            let flat = new Array()
            for (var i = 0; i < arr.length; i++) {
                flat.push(arr[i][0])
                flat.push('z down move')
                flat = flat.concat(arr[i].slice(1))
                flat.push('z up move')
            }
            // console.log('FLATTENED TO', flat)
            // add header
            flat.splice(0, 0, 'z up move')
            // and return home 
            flat.push([0, 0])
            // make a list of objs 
            let list = new Array()
            // this is actually stateful ... jogging or not 
            let upState = false
            for (var j = 0; j < flat.length; j++) {
                if (Array.isArray(flat[j])) {
                    let move = {
                        position: {
                            X: flat[j][0] / state.unitsPerMM,
                            Y: flat[j][1] / state.unitsPerMM
                        }
                    }
                    if (upState) {
                        move.speed = state.jogSpeed
                    } else {
                        move.speed = state.moveSpeed
                    }
                    list.push(move)
                } else if (flat[j] === 'z up move') {
                    upState = true
                    list.push({
                        position: {
                            Z: state.lift
                        },
                        speed: state.plungeSpeed
                    })
                } else if (flat[j] === 'z down move') {
                    upState = false
                    list.push({
                        position: {
                            Z: 0
                        },
                        speed: state.plungeSpeed
                    })
                }
            }

            return list

        } else {
            console.log('NEIL PARSE: err: not an array')
        }

    }

    function onShift() {
        if (npList != null && npList.length > 0) {
            neilParser.outputs.move.emit(npList.shift())
            state.listLength = npList.length
        } else {
            console.log('NP Completes Moves')
        }
    }

    return neilParser
}


// export the module 
module.exports = NeilParser