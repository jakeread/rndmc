// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI 

// descartes, to you 
const DCRT = require('../../src/cartesian.js')
const MJS = require('mathjs')

// planner consumes target moves (i.e. segments having uniform speed throughout)
// and subjects them to acceleration constraints 

function Planner() {
    var planner = {
        description: {
            name: 'Lookahead-Motion-Planner',
            alt: 'movements -> acceleration planned moves'
        }
    }

    // log more 
    var verbose = false 

    planner.state = State()
    var state = planner.state // reference pass attempt?

    state.axisIDs = 'X,Y,Z'
    state.onUiChange('axisIDs', axisIDUpdate)

    state.accel = 200 // units/s/s
    state.jd = 0.1 // units to arc about
    state.minSpeed = 1 // units/s

    state.position = [0, 0, 0]

    // should be grn / red button ... 
    state.isRunning = 1
    state.onUiChange('isRunning', netStateRefresh)

    state.netWindow = 3
    state.netState = [0, 0, 0]

    planner.ui = UI()
    var ui = planner.ui 
    ui.addElement('resetButton', 'ui/uiButton.js')
    ui.resetButton.subscribe('onload', function(msg){
        ui.resetButton.send({
            calls: 'setText',
            argument: 'reset planner'
        })
    })
    ui.resetButton.subscribe('onclick', onPlannerReset)

    ui.addElement('startStopButton', 'ui/uiButton.js')
    ui.startStopButton.subscribe('onload', function(msg){
        ui.startStopButton.send({
            calls: 'setText',
            argument: 'start / stop planner'
        })
    })
    ui.startStopButton.subscribe('onclick', onStartStop)

    planner.inputs = {
        instruction: Input('move instruction', onNewInstruction),
        acks: Input('move acknowledgement', onAck),
        run: Input('boolean', onRunInstruction)
    }

    planner.outputs = {
        moves: Output('move instruction'),
        moveComplete: Output('number')
    }

    // we'll use one of these to assert / do things 
    // after the module is loaded, and state is copied etc
    // i.e. one thing we can do is assert a starting value 
    planner.init = function(){
        state.isRunning = 0 
        state.netState = [0, 0, 0]
    }

    /*
    ------------------------------------------------------
    UPDATING / SETUP
    ------------------------------------------------------
    */

    function onAck(msg) {
        //console.log("Planner onAck:", msg)
        // update position, net states, run netCheck
        var axes = state.axisIDs.split(',')
        var match = axes.indexOf(msg.axis)
        if (match !== -1) {
            // HERE is a hack, for our state-updating system doesn't yet accomodate arrays 
            var newPos = state.position.slice(0)
            newPos[match] += msg.increment
            state.position = newPos
            var newNetState = state.netState.slice(0)
            newNetState[match] -= 1
            state.netState = newNetState
            if(verbose) console.log('NEW NET STATE', newNetState)
        } else {
            console.log('ERR in PLANNER: missed axis on ack from stepper')
        }
        netStateRefresh()
    }

    function onRunInstruction(boolean) {
        if (boolean) {
            if (state.isRunning) {
                // already running, do nothing 
            } else {
                state.isRunning = 1
                netStateRefresh()
            }
        } else {
            if (state.isRunning) {
                state.isRunning = 0
            } else {
                // oy 
            }
        }
    }

    function axisIDUpdate() {
        var axes = state.axisIDs.split(',')
        var position = new Array(axes.length)
        position.fill(0)
        var packetState = new Array(axes.length)
        for (i in axes) {
            position.push(0)
            packetState.push(0)
            // could do 
            //planner.outputs[axes[i]] = Output('move instruction')
        }
        state.position = position
        state.packetState = packetState
        console.log(planner)
    }

    function onPlannerReset() {
        console.log("RESET PLANNER NOT YET IMPLEMENTED")
    }

    function onStartStop() {
        if (state.isRunning) {
            state.isRunning = 0
        } else {
            state.isRunning = 1
            netStateRefresh()
        }
    }

    function netStateRefresh() {
        // if there are less packets on the network than our window
        // and the planner is running
        // send a packet 
        if (state.isRunning) {
            var ns = state.netState
            var i = 0
            // check equality
            while (ns[i] == ns[i + 1]) {
                i++
                // console.log('EQUALITY LOOP')
                if (i > ns.length - 1) {
                    break
                }
            }
            if (i == ns.length - 1) {
                while (ns[i] < state.netWindow) {
                    // console.log('loop 2')
                    sendMoveToNetwork()
                    if (mq.length <= 0) {
                        planner.isRunning = 0
                        break
                    }
                }
            }
        }
    }

    function sendMoveToNetwork() {
        if (mq.length > 0) {
            for (i in state.netState) {
                state.netState[i]++
            }
            state.netState = state.netState
            // dereference
            var move = JSON.parse(JSON.stringify(mq.shift()))
            if(verbose) console.log("MOVE FROM PLANNER", move)
            planner.outputs.moves.emit(move)
            planner.outputs.moveComplete.emit(1)
        } else {
            planner.isRunning = 0
        }
    }

    /*
    ------------------------------------------------------
    ENTRY POINTS
    ------------------------------------------------------
    */

    // the move que, of which it is nice to have shorthand 
    var mq = new Array()

    var zlcounter = 0

    function onNewInstruction(move) {
        // our axis
        var axes = state.axisIDs.split(',')

        // we'll make a new move object
        // start and end points, and axes to track
        var p1 = []
        var p2 = new Array(axes.length)
        p2.fill(0)
        // start point is current pos. if no other moves
        // otherwise it's the tail of the last move
        if (mq.length === 0) {
            p1 = state.position
        } else {
            p1 = mq[mq.length - 1].p2
        }
        // now pick out new deltas
        for (i in axes) {
            var key = axes[i]
            if (move.position[key] != null) {
                // some new pos,
                p2[i] = move.position[key]
            } else {
                // or none, this axis stays 
                p2[i] = p1[i]
            }
        }
        // check for zero-length vector 
        if (MJS.distance(p1, p2) == 0) {
            console.log('------------------ !ACHTUNG! -------------------')
            console.log('------------------ !ACHTUNG! -------------------')
            console.log('planner throwing zero length vector')
            zlcounter ++
            console.log(zlcounter)
            // this means we need another one from the queue
            planner.outputs.moveComplete.emit(1)
        } else {
            // starting with basics
            var newMove = {
                axes: axes,
                p1: p1,
                p2: p2,
                cruise: move.speed,
                entry: 0,
                exit: 0,
                accel: state.accel
            }
            // add it to the queue
            mq.push(newMove)
            // run the planner
            runJD()
            // this will check current network / windowed state, and send 
            netStateRefresh()
        }
    }

    /*
    ------------------------------------------------------
    JUNCTION DEVIATION 
    walk mq to generate permissible entry / exit speeds 
    by 'junction deviation' algorithm 
    ------------------------------------------------------
    */

    function runJD() {
        // useful
        var accel = state.accel
        var jd = state.jd
        var ms = state.minSpeed
        // should reference axis ids, not these moves ? 
        var numAxis = mq[0].p1.length

        // we always plan for a full stop at the end of the queue
        // ('full stop' == minimum speed)
        mq[mq.length - 1].exit = ms

        // start condition is unclear to me 
        if (mq.length < 2) {
            mq[0].entry = ms
        } else {
            // we gucci, run a pass and set exit / entry speeds
            // to maximum permissible by JD 
            // because we know the final exit speed, we'll run the pass from
            // back to start 
            if(verbose) console.log('REV PASS')
            for (var i = mq.length - 1; i > 0; i--) {
                // the moves we're junctioning around / transitioning between 
                var mqi = mq[i - 1]
                var mqn = mq[i]
                // vectors
                var vEnter = MJS.subtract(mqi.p2, mqi.p1)
                var vExit = MJS.subtract(mqn.p2, mqn.p1)
                // max. permissible speed at meeting point, by jd 
                var vJunc = calcJunctionSpeed(vEnter, vExit, jd, accel, false)
                //console.log(vJunc)
                // in this backwards bass, vJunc is now our best candidate for the junction speed
                // i.e. the speed at the end of mqi and the start of mqn
                var vMaxEntry = calcMaxEntry(mqn, accel, false)
                //console.log(vMaxEntry)
                // take the min of 
                // the maximum possible entry w/r/t exit, 
                // the the allowable junction speed,
                var cornerAtV = Math.min(vMaxEntry, vJunc)
                // and assert this greater than our minimum speed 
                var cornerAtV = Math.max(ms, cornerAtV)
                // make this cornering speed the entry speed for this move,
                // the exit speed for the previous move, and then step back
                mqn.entry = cornerAtV
                mqi.exit = cornerAtV
            }
            // we've swept backwards to make sure we can decelerate into the end point,
            // now we need to make sure we can accelerate up to these cornering velocities
            // and if not, set the exit speeds appropriately, so that we can get there
            if(verbose) console.log('FWD PASS')
            for (var i = 0; i < mq.length - 1; i++) {
                var mqi = mq[i]
                var mqn = mq[i + 1]
                var vMaxExit = calcMaxExit(mqi, accel, false)
                // and assert this cornering speed to not be greater than our maximum exit 
                // by walking all the way forwards, now we guarantee that we can reach, but won't
                // exceed, the cornering speeds we set in the reverse pass 
                var cornerAtV = Math.min(mqi.exit, vMaxExit)
                // and assert that this is greater than our minimum speed 
                var cornerAtV = Math.max(ms, cornerAtV)
                // then this exit is the exit speed of this move, the entrance to the next
                mqi.exit = cornerAtV
                mqn.entry = cornerAtV
            }
            // now that we know permissible cornering speeds,
            // we'll calculate the trapezoid shapes
            // this becomes very useful for turning moves over to stepper motors
            // and will give us a time estimate for each move as well,
            // we need / want that so that we can set a network buffer length 
            if(true) console.log('PLANNER: trapezoid times in queue')
            for (var i = 0; i < mq.length; i++) {
                calcTrap(mq[i], accel, false)
                if (moveTime(mq[i]) < 0.1) {
                    console.log('------------------ !ACHTUNG! -------------------')
                    console.log('------------------ !ACHTUNG! -------------------')
                    console.log("WARN! move time here", moveTime(mq[i]))
                }
                console.log(moveTime(mq[i]))
            }
        }
    }

    function calcMaxEntry(move, a, debug) {
        // maximum entry speed given distance, vf and a
        // vf^2 = vi^2 + 2*a*d
        var vf = move.exit
        var vec = MJS.subtract(move.p2, move.p1)
        var d = DCRT.length(vec)
        var viMax = Math.sqrt(Math.pow(vf, 2) + 2 * a * d)
        if (debug) console.log('viMax', viMax)
        return viMax
    }

    function calcMaxExit(move, a, debug) {
        var vi = move.entry
        var vec = MJS.subtract(move.p2, move.p1)
        var d = DCRT.length(vec)
        var vfMax = Math.sqrt(Math.pow(vi, 2) + 2 * a * d)
        if (debug) console.log('vfMax', vfMax)
        return vfMax
    }

    function calcJunctionSpeed(v1, v2, jd, a, debug) {
        // entry and exit vectors, not unitized 
        if (debug) console.log('vectors for jd', v1, v2)
        // dot product of these is their magnitudes * cosine of the angle between them 
        var dot = MJS.dot(v1, v2) / (DCRT.length(v1) * DCRT.length(v2))
        // we want the reciprocal of this 
        var omega = Math.PI - Math.acos(dot)
        if (debug) console.log('angle betwixt', DCRT.degrees(omega))
        // our angle for secant makes up the other third of a right-triangle with half of omega 
        // i.e. together they are 90 deg 
        var theta = Math.PI / 2 - omega / 2
        if (debug) console.log('angle for secant', DCRT.degrees(theta))
        // r and d, when r is 1
        var rd = 1 / Math.cos(theta)
        if (debug) console.log('rd', rd)
        // r proportional for given d 
        var r = jd / (rd - 1)
        if (debug) console.log('r', r)
        // permissible junction velocity for limit of accel, and radius
        var v = Math.sqrt(a * r)
        if (debug) console.log('junction v', v)
        return v
    }

    /*
    ------------------------------------------------------
    TRAPEZOIDS FROM JUNCTION DEVIATION ENTRY / EXITS
    ------------------------------------------------------
    */

    function calcTrap(move, a, debug) {
        // delta is signed ! important for writing direction later, we'll use delta for maths
        if (debug) console.log(move)
        // have p1, p2, cruise, entry, exit
        // add length, figure if full-accel, full-deccel, triangle, trapezoid, cruise ...
        move.vector = MJS.subtract(move.p2, move.p1)
        var d = DCRT.length(move.vector)
        var vi = move.entry
        var v = move.cruise
        var vf = move.exit
        // limits
        var maxExit = Math.sqrt(Math.pow(vi, 2) + 2 * a * d)
        var maxEntry = Math.sqrt(Math.pow(vf, 2) + 2 * a * d)
        // seven possible cases
        // HERE set move with type, accel / deccel lengths, calculate time for each, and total time 
        // then get on with writing a larger gcode window, bringing in some 'real' moves, and getting these
        // out / back from steppers 
        if (d <= 0) {
            console.log('ZERO LENGTH MOVE')
        } else if (maxExit <= vf) {
            if (debug) console.log('TRAP: full ramp up');
            move.type = 'ramp up'
            // full ramp up
            // accel, cruise, and deccel time 
            move.t1 = (vf - vi) / a
            move.d1 = d
            move.t2 = 0
            move.d2 = 0
            move.t3 = 0
            move.d3 = 0
            // check we won't break
            if (maxExit < vf) console.log('max ramp up, misses by', vf - maxExit)
        } else if (maxEntry <= vi) {
            if (debug) console.log('TRAP: full ramp down');
            move.type = 'ramp down'
            move.t1 = 0
            move.d1 = 0
            move.t2 = 0
            move.d2 = 0
            move.t3 = (vi - vf) / a
            move.d3 = d
            // check we're not going to break / not make
            if (maxEntry < vi) console.log('max ramp down, misses by', vi - maxEntry)
        } else if (vi == v && vf == v) {
            if (debug) console.log('TRAP: full cruise');
            move.type = 'cruise'
            move.t1 = 0
            move.d1 = 0
            move.t2 = d / v
            move.d2 = d
            move.t3 = 0
            move.d3 = 0
        } else if (vi == v) {
            if (debug) console.log('TRAP: cruise, decelerate');
            move.type = 'cruise and deccel'
            var deccelDistance = (Math.pow(v, 2) - Math.pow(vf, 2)) / (2 * a)
            move.t1 = 0
            move.d1 = 0
            move.t2 = (d - deccelDistance) / v
            move.d2 = d - deccelDistance
            move.t3 = (v - vf) / a
            move.d3 = deccelDistance
        } else if (vf == v) {
            if (debug) console.log('TRAP: accel, cruise');
            move.type = 'accel and cruise'
            var accelDistance = (Math.pow(v, 2) - Math.pow(vi, 2)) / (2 * a)
            move.t1 = (v - vi) / a
            move.d1 = accelDistance
            move.t2 = (d - accelDistance) / v
            move.d2 = d - accelDistance
            move.t3 = 0
            move.d3 = 0
        } else {
            // either full trapezoid, or a triangle 
            // distances to / from cruise from / to start / end 
            var accelDistance = (Math.pow(v, 2) - Math.pow(vi, 2)) / (2 * a)
            var deccelDistance = (Math.pow(v, 2) - Math.pow(vf, 2)) / (2 * a)
            if (accelDistance + deccelDistance < d) {
                if (debug) console.log('TRAP: complete trapezoid');
                move.type = 'complete trapezoid'
                move.t1 = (v - vi) / a
                move.d1 = accelDistance
                move.t2 = (d - accelDistance - deccelDistance) / v
                move.d2 = d - accelDistance - deccelDistance
                move.t3 = (v - vf) / a
                move.d3 = deccelDistance
            } else {
                if (debug) console.log('TRAP: triangular');
                // find the peak 
                var vPeak = Math.sqrt(((2 * a * d + Math.pow(vi, 2) + Math.pow(vf, 2)) / 2))
                var midPoint = (Math.pow(vPeak, 2) - Math.pow(vi, 2)) / (2 * a)
                move.type = 'triangular'
                move.t1 = (v - vi) / a
                move.d1 = midPoint
                move.t2 = 0
                move.d2 = 0
                move.t3 = (v - vf) / a
                move.d3 = d - midPoint
            }
        }
    }

    function moveTime(move) {
        if (move.t1 != null) {
            return move.t1 + move.t2 + move.t3
        } else {
            console.log('move with no time')
        }
    }

    // init ? 
    onPlannerReset()

    return planner
}

module.exports = Planner