// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State
let Button = JSUnit.Button

const Hardware = require('../../src/atkunit.js')
const PCKT = require('../../src/packets.js')

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI

// a constructor, a fn, a javascript mess
function ATKMathRobot() {

    // TODO:
    // add small filter to PID / D-term 
    // add deadband option?
    // do chart-tune 
    // tune ... and take video
    // do three.js 

    // we make the module, starting from this base 
    // '0,1' is the route to our piece of hardware
    // 'onPacket' is the function that will be called
    // when data arrives from that port 
    var atkmr = Hardware()

    // change the name ... 
    atkmr.description.name = 'atk-math-robot-joint'

    // inputs and outputs as usual 
    atkmr.inputs = {
        tick: Input('event', onPositionTickTock),
        set_pc_t: Input('number', onPositionTargetInput),
        get_pos: Input('event', onPositionRequest)
    }
    atkmr.outputs = {
        ok: Output('nothing-yet'),
        pos: Output('uint16_t')
    }

    // and state as well 
    var state = atkmr.state

    state.message = 'no packet yet'
    state.enc_cnt = 16384
    state.enc_offset = 0
    state.enc_reverse = false 
    state.pc_r = 0
    state.pc_t = 2048
    state.onUiChange('pc_t', onPositionTargetUserChange)

    state.pKp = 10
    state.onUiChange('pKp', onKValsUpdate)
    state.pKi = 0.005
    state.onUiChange('pKi', onKValsUpdate)
    state.pKd = 40.0
    state.onUiChange('pKd', onKValsUpdate)
    state.cKp = 4.0
    state.onUiChange('cKp', onKValsUpdate)
    state.cKi = 0.0
    state.onUiChange('cKi', onKValsUpdate)

    state.walk = 1024

    atkmr.ui = UI()
    var ui = atkmr.ui
    ui.addElement('walkValButton', 'ui/uiButton.js')
    ui.walkValButton.subscribe('onload', function(msg){
        ui.walkValButton.call('setText', 'click to walk val')
    })
    ui.walkValButton.subscribe('onclick', onPositionTickTock)

    // to send things down the well, we can use
    // atkmr.route.send(packet) 
    // where packet is a byte array

    function onKValsUpdate() {
        var pckt = [144]
        pckt = pckt.concat(PCKT.packFloatTo32(state.pKp, true), PCKT.packFloatTo32(state.pKi), PCKT.packFloatTo32(state.pKd), PCKT.packFloatTo32(state.cKp), PCKT.packFloatTo32(state.cKi))
        console.log('pckt like', pckt)
        atkmr.route.send(pckt)
    }

    function onPositionTargetInput(evt) {
        console.log("INPUTS NOT YET BOUND", evt)
    }

    function onPositionRequest(evt){
        var pckt = [145]
        atkmr.route.send(pckt)
    }

    atkmr.route.subscribe(145, function(msg){
        var pos = PCKT.unPack32(msg.slice(1))
        state.pc_r = pos 
        if(state.enc_reverse){
            pos = state.enc_cnt - pos 
        }
        pos += state.enc_offset
        pos %= state.enc_cnt 
        var rads = 2*Math.PI * (pos / state.enc_cnt)
        atkmr.outputs.pos.emit(rads)
    })

    function onPositionTargetUserChange() {
        var pc_t = state.pc_t
        if (pc_t > 16384) {
            pc_t = 16384
        } else if (pc_t < 0) {
            pc_t = 0
        }

        var pc_tpack = PCKT.pack32(pc_t)
        pc_tpack.unshift(143)

        state.message = 'packet out'
        atkmr.route.send(pc_tpack)
    }

    function onPositionTickTock() {
        var pc_t = state.pc_t
        pc_t += state.walk
        pc_t %= 16384
        state.pc_t = pc_t
        onPositionTargetUserChange()
    }

    // to get replies to certain packets, we can
    // subscribe, where 141 (here) is the 'key' 
    // we're looking for on the network, and the 
    // msg is byte array we'll get back
    atkmr.route.subscribe(143, function(msg) {
        state.message = 'packet ok'
    })

    return atkmr
}

// exports 
module.exports = ATKMathRobot