// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State
let Button = JSUnit.Button

const Hardware = require('../../src/atkunit.js')
const PCKT = require('../../src/packets.js')

// a constructor, a fn, a javascript mess
function ATKBreadBoardBoard() {

    // we make the module, starting from this base 
    // '0,1' is the route to our piece of hardware
    // 'onPacket' is the function that will be called
    // when data arrives from that port 
    var atkbbb = Hardware()

    // change the name ... 
    atkbbb.description.name = 'ATKBBB-Servo'

    // inputs and outputs as usual 
    atkbbb.inputs = {
        servoVal: Input('number', onServoValChange),
        adcRequest: Input('event', onADCRequest)
    }
    atkbbb.outputs = {
        servoConf: Output('event'),
        adcValue: Output('number')
    }

    // and state as well 
    var state = atkbbb.state 

    state.servoVal = 0 // 0->100 does 1 -> 2ms duty on 20ms period
    state.onUiChange('servoVal', onServoValChange)

    var ui = atkbbb.ui 
    ui.addElement('servoButton', 'ui/uiButton.js')
    ui.servoButton.subscribe('onload', function(msg){
        ui.servoButton.send({
            calls: 'setText',
            argument: 'click to send servo value'
        })
    })
    ui.servoButton.subscribe('onclick', onServoValChange)

    ui.addElement('adcReqButton', 'ui/uiButton.js')
    ui.adcReqButton.subscribe('onload', function(msg){
        ui.adcReqButton.send({
            calls: 'setText',
            argument: 'click to request adc conversion'
        })
    })
    ui.adcReqButton.subscribe('onclick', onADCRequest)

    // to send things down the well, we can use
    // atkbbb.route.send(packet) 
    // where packet is a byte array

    function onServoValChange(evt) {
        var pwm = state.servoVal
        if (pwm > 100) {
            pwm = 100
        } else if (pwm < 0) {
            pwm = 0
        }

        var microval = Math.round(7.5 * pwm)
        console.log('pwm on line', microval)

        var pwmpack = PCKT.pack32(microval)
        pwmpack.unshift(141)

        atkbbb.route.send(pwmpack)
    }

    // to get replies to certain packets, we can
    // subscribe, where 141 (here) is the 'key' 
    // we're looking for on the network, and the 
    // msg is byte array we'll get back
    atkbbb.route.subscribe(141, function(msg){
        atkbbb.outputs.servoConf.emit('1')
    })

    function onADCRequest(evt){
        var pckt = new Array() 
        pckt.push(142)
        atkbbb.route.send(pckt)
    }

    atkbbb.route.subscribe(142, function(msg){
        // working on this ... 
        console.log('ADC Request Response', msg)
        atkbbb.outputs.adcValue.emit(msg)
    })

    return atkbbb
}

// exports 
module.exports = ATKBreadBoardBoard