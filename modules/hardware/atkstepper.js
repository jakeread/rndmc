// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output

let State = JSUnit.State

const MJS = require('mathjs')
const DCRT = require('../../src/cartesian.js')

const Hardware = require('../../src/atkunit.js')
const PCKT = require('../../src/packets.js')

function Stepper() {

    var stepper = Hardware()

    // log more 
    var verbose = false 

    stepper.description.name = 'ATKStepper'
    stepper.description.alt = 'software representation of stepper motor'

    stepper.inputs = {
        trapezoid: Input('move instruction', onNewInstruction),
        accel: Input('number', onAccelCommand),
        rmtrig: Input('event', onRawMove)
    }

    stepper.outputs = {
        ack: Output('move acknowledgement'),
        position: Output('number')
    }

    // ptr to state (from Hardware() beginnings)
    var state = stepper.state

    // for acceleration moves, in steps/s/s 
    state.rate = 2000

    state.onUiChange('rate', () => {
        if(state.rate > 2000){
            state.rate = 2000 
        } else if (state.rate < 100){
            state.rate = 100 
        }
    })

    state.axis = 'X'
    state.spu = 80 // steps per unit 
    state.rawMove = -10

    var ui = stepper.ui 
    ui.addElement('rawButton', 'ui/uiButton.js')
    ui.rawButton.subscribe('onload', function(msg){
        ui.rawButton.send({
            calls: 'setText',
            argument: 'click to send test move'
        })
    })
    ui.rawButton.subscribe('onclick', onRawMove)

    state.lead = 0
    state.position = 0 // in steps 

    function onRawMove() {
        console.log('TEST move for', state.rawMove)
        // finds type of ramp, calculates entry and exit ramp lengths 
        var len = Math.abs(state.rawMove)
        var delta = state.rawMove
        var testMove = {
            delta: delta,
            entry: 5,
            accel: 10,
            accelLen: len / 2 - len / 4,
            deccelLen: len / 2 + len / 4
        }
        sendToHardware(testMove)
    }

    function onAccelCommand(num){
        // 1 or 0 for start or stop accelerating 
    }

    function onNewInstruction(move) {
        // console.log('move to stepper', stepper.state.axis, move)
        // pick out axis (check if it's a wait move)
        // like

        // axis values
        var axisIndex = move.axes.indexOf(state.axis)
        //console.log('axisIndex', axisIndex, state.axis)
        if (axisIndex == -1) {
            console.log('STEPPER AXIS NOT FOUND IN MOVE')
        } else {
            // have to watch out for zero-lengths on this axis, also 
            var delta = move.vector[axisIndex]
            //console.log('DELTA', delta)
            if (Math.abs(delta) <= 0) {
                // find a DOF w/o zlv
                var fnd = false
                var altAxis = null
                for (i in move.p1) {
                    if (Math.abs(move.vector[i]) > 0) {
                        altAxis = i
                        break
                    }
                }
                if (altAxis != null) {
                    if(verbose) console.log('ALTAXIS', altAxis)
                    var axisMove = pullAxis(move, i)
                    axisMove.isWait = true
                    sendToHardware(axisMove)
                } else {
                    console.log("ZERO LENGTH INTO STEPPER")
                }
            } else {
                var axisMove = pullAxis(move, axisIndex)
                axisMove.isWait = false
                sendToHardware(axisMove)
            }
        }
    }

    function pullAxis(move, i) {
        // like
        /*
        var move = {
            p1, p2,
            cruise, entry, exit,
            vector,
            type, 
            t1, d1, t2, d2, t3, d3
        }
        */
        /*
        var pckt = {
            steps,
            entry, 
            accel,
            accelLen,
            deccelLen
        }
        */
        var delta = move.p2[i] - move.p1[i]
        var frLen = DCRT.length(move.vector)
        var rel = Math.abs(delta) / frLen

        // for stepper hardware, we want
        // total length of move
        // entry speed, acceleration rate (up and down)
        // # of steps for which to accelerate
        // # of steps *after which* to begin deccelerating 
        // in this format, firmware is very simple: we count 

        var entry = move.entry * rel
        var accel = move.accel * rel
        var accelLen = move.d1 * rel
        var deccelLen = (move.d1 + move.d2) * rel

        var aMove = {
            delta: delta,
            entry: entry,
            accel: accel,
            accelLen: accelLen,
            deccelLen: deccelLen
        }

        return aMove
    }

    /*

    SOFTWARE -> HARDWARE ---------------------------------------------------

    */

    function sendToHardware(aMove) {
        // steps / unit
        var spu = state.spu
        var convert = Math.abs(spu)
        // 'axis' move and discreet move
        var dMove = {}
        dMove.steps = watchRounding(aMove.delta * spu)
        dMove.entry = watchRounding(aMove.entry * convert)
        dMove.accel = watchRounding(aMove.accel * convert)
        dMove.accelLen = Math.round(aMove.accelLen * convert)
        dMove.deccelLen = Math.round(aMove.deccelLen * convert)
        if (Math.abs(dMove.steps) < 2) {
            console.log("!ACHTUNG! ------------------------------------- VERY SMALL STEP MOVE")
            console.log(dMove)
        }

        if(true) console.log('------------------- DMOVE', state.axis, dMove.steps)

        var packet = new Array()
        // step blocks are #131
        // wait blocks are #132
        if (aMove.isWait) {
            packet.push(132)
        } else {
            packet.push(131)
        }
        packet = packet.concat(PCKT.pack32(dMove.steps))
        if(verbose) console.log('------------------- PMOVE', packet)
        packet = packet.concat(PCKT.pack32(dMove.entry))
        packet = packet.concat(PCKT.pack32(dMove.accel))
        packet = packet.concat(PCKT.pack32(dMove.accelLen))
        packet = packet.concat(PCKT.pack32(dMove.deccelLen))

        state.lead = state.lead + dMove.steps
        stepper.route.send(packet)
    }

    function watchRounding(val) {
        var rounded = Math.round(val)
        if (rounded == 0) {
            // console.log('WATCH ZERO', val, rounded)
        }
        var error = Math.abs(val / rounded - 1)
        if (error > 0.05) {
            console.log('WATCH ROUNDING', val, rounded, error)
        }
        return rounded
    }

    stepper.route.subscribe(131, onHardwareStepsComplete)

    function onHardwareStepsComplete(pckt) {
        var stepsMade = PCKT.unPack32(pckt.slice(1))
        state.position += stepsMade
        var unitsMade = stepsMade / state.spu
        var ack = {
            axis: state.axis,
            increment: unitsMade
        }
        if(verbose) console.log("STEPPER ACK MOVE", state.axis, stepsMade)
        stepper.outputs.ack.emit(ack)
        stepper.outputs.position.emit(state.position)
    }

    stepper.route.subscribe(132, onHardwareWaitComplete)

    function onHardwareWaitComplete(pckt) {
        //var msg = PCKT.unPack32(pckt)
        var ack = {
            axis: state.axis,
            increment: 0
        }
        if(verbose) console.log("STEPPER ACK WAIT", state.axis)
        stepper.outputs.ack.emit(ack)
    }

    return stepper
}

module.exports = Stepper