// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State
let Button = JSUnit.Button

var NodeWebcam = require("node-webcam")

// C920 doesn't work with windows command line util 
// this module needs love / work / a working install of opencv 

var wcOptions = {
    width: 1920,
    height: 1080,
    quality: 100,
    delay: 0,
    saveShots: true,
    output: 'jpeg',
    device: false,
    callbackReturn: 'location',
    verbose: true
}

// a constructor, a fn, a javascript mess
function WEBCAM() {

    var webcam = {
        // descriptions are used in UI
        description: {
            name: 'CAMERA Request',
            alt: 'webcam',
            isHardware: true
        }
    }

    //var camera = NodeWebcam.create(wcOptions)

    var wc = NodeWebcam.create(wcOptions)

    webcam.state = State()
    // alias !
    var state = webcam.state

    state.button = Button('REQUEST IMAGE')
    state.onUiChange('button', onButtonPress)
    state.counter = 0

    webcam.inputs = {
        trigger: Input('event', onButtonPress) // makes anything into '1' event
    }

    webcam.outputs = {
        image: Output('image'),
        callback: Output('event')
    }

    function onButtonPress(evt) {
        console.log("TAKING PHOTO")
        state.counter ++
        wc.capture('save/' + 'pc' + state.counter.toString(), function(err, data) {
            if (err != null) {
                console.log("CAM ERR ---------------")
                console.log(err)
            } else {
                console.log("photo ok")
                webcam.outputs.callback.emit(true)
            }
        });
    }

    return webcam
}

// exports 
module.exports = WEBCAM