// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

function Gcode() {

    var gcode = {
        description: {
            name: 'Gcode Parser',
            alt: 'line of gcode -> planner recognized move'
        }
    }

    // log more 
    var verbose = false 

    // one caveat here is that we can't dynamically add objects to this, 
    // or they don't get getter / settered when we do
    gcode.state = State()
    gcode.state.mode = 'G0'
    gcode.state.G0 = 1200
    gcode.state.G1 = 400

    gcode.inputs = {
        lineIn: Input('string', onLineIn)
    }

    gcode.outputs = {
        instructionOut: Output('move instruction'),
        modeChange: Output('string')
    }

    // input functions
    function onLineIn(str){
        var instruction = parseGcode(str)
        if (instruction.hasMove) {
            if(verbose) console.log('GCODE:', instruction)
            gcode.outputs.instructionOut.emit(instruction)
        } else {
            if(verbose) console.log('GCODE:', gcode.state.mode)
            gcode.outputs.modeChange.emit(gcode.state.mode)
        }
    }

    // local functions
    function getKeyValues(str) {
        var kv = {}
        for (var i = 0; i < str.length; i++) {
            if (str[i].match('[A-Za-z]')) { // regex to match upper case letters
                var lastIndex = str.indexOf(' ', i)
                if (lastIndex < 0) {
                    lastIndex = str.length
                }
                var key = str[i].toUpperCase()
                kv[key] = parseFloat(str.slice(i + 1, lastIndex))
            }
        }
        return kv
    }

    // TODO: test, can we link global vars to ui objects ... 
    // gcode.ui.mode.value = var ? no bc set / get etc 
    // more like var = gcode.ui.mode.value ? is this referential?

    function parseGcode(str) {
        var instruction = {
            position: {},
            hasMove: false,
            speed: 0
        }

        kv = getKeyValues(str)
        // track modality
        if (kv.G == 0 | kv.G == 1) {
            gcode.state.mode = 'G' + kv.G.toString()
        } else if (kv.G != null) {
            // no arcs pls
            console.log('unfriendly Gcode mode!', kv)
        }

        for (key in kv) {
            if (key.match('[A-EX-Z]')) {
                instruction.position[key] = kv[key]
                instruction.hasMove = true
            } else if (key.match('[F]')) {
                // tricky / ugly: sets using the mode state string as object key 
                gcode.state[gcode.state.mode] = kv.F
            }
        }

        instruction.speed = gcode.state[gcode.state.mode]
        // and this for help later?
        instruction.kv = kv

        return instruction
    }

    return gcode

}


// export the module 
module.exports = Gcode