// boilerplate rndmc header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI 

// a constructor, a fn, a javascript mess
function uiNum() {

    // this is the tiny program-as-and-object that we'll load into rundmc 
    // description / name is required to load successfully 
    var uinum = {
        description: {
            name: 'number-output',
            alt: 'for clicking'
        }
    }

    // the State() object is what the system scrapes for ui variables / updates from the UI
    // this includes things like Button('title', callback), which are unique state variables
    // they can also be found in jsunit.js 
    uinum.state = State()
    // alias !
    var state = uinum.state 

    state.number = 10

    uinum.ui = UI() 
    var ui = uinum.ui 
    ui.addElement('onNumberButton', 'ui/uiButton.js')
    ui.onNumberButton.subscribe('onload', function(msg){
        ui.onNumberButton.send({
            calls: 'setText',
            argument: 'number out ->'
        })
    })
    ui.onNumberButton.subscribe('onclick', onNumberDesire)

    // inputs are required, and must be Input('type', callback) 
    uinum.inputs = {
        thru: Input('any', onThruInput), // makes anything into num event 
        evt: Input('any', onNumberDesire)
    }

    // outputs: Output('type')
    uinum.outputs = {
        out: Output('number')
    }

    // here's our input callback, specified in the input constructor 
    function onThruInput(input){
        if(typeof input == 'number'){
            state.number = input
        } else {
            state.number = parseFloat(input)
        }
        onNumberDesire()
    }

    function onNumberDesire(){
        // here's how we fire an output. 
        uinum.outputs.out.emit(state.number)
    }

    // gotta give the program this thing we made 
    return uinum
}

// this for node.js's require() function 
module.exports = uiNum