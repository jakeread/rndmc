// boilerplate atkapi header
// boilerplate header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// a constructor, a fn, a javascript mess
function Delay() {

    var delay = {
        // descriptions are used in UI
        description: {
            name: 'delay',
            alt: 'in ... out'
        }
    }

    delay.state = State()
    // alias !
    var state = delay.state

    state.ms = 100
    state.onUiChange('ms', onMsChange)

    delay.inputs = {
        thru: Input('any', onDelayBegin) // makes anything into '1' event
    }

    delay.outputs = {
        out: Output('any')
    }

    function onMsChange(){
        // a test fn
        console.log("noting state change", state.ms)
    }

    function onDelayBegin(input){
        setTimeout(function(){
            delay.outputs.out.emit(input)
        }, state.ms)
    }

    return delay
}

// exports 
module.exports = Delay