// boilerplate rndmc header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI 

// a constructor, a fn, a javascript mess
function UIArray() {

    // this is the tiny program-as-and-object that we'll load into rundmc 
    // description / name is required to load successfully 
    var uiArray = {
        description: {
            name: 'array-output',
            alt: 'for clicking'
        }
    }

    // the State() object is what the system scrapes for ui variables / updates from the UI
    // this includes things like Button('title', callback), which are unique state variables
    // they can also be found in jsunit.js 
    uiArray.state = State()
    // alias !
    var state = uiArray.state 

    state.array = [1.2,5.5,7.1]

    uiArray.ui = UI() 
    var ui = uiArray.ui 
    ui.addElement('onArrayButton', 'ui/uiButton.js')
    ui.onArrayButton.subscribe('onload', function(msg){
        ui.onArrayButton.send({
            calls: 'setText',
            argument: 'array out ->'
        })
    })
    ui.onArrayButton.subscribe('onclick', onArrayDesire)

    // inputs are required, and must be Input('type', callback) 
    uiArray.inputs = {
        thru: Input('any', onThruInput), // makes anything into num event 
        evt: Input('any', onArrayDesire)
    }

    // outputs: Output('type')
    uiArray.outputs = {
        out: Output('number')
    }

    // here's our input callback, specified in the input constructor 
    function onThruInput(input){
        if(Array.isArray(input)){
            state.array = input
             onArrayDesire()
        } else {
            console.log("ERR input to array module is non-array")
        }
    }

    function onArrayDesire(){
        // here's how we fire an output. 
        uiArray.outputs.out.emit(state.array)
    }

    // gotta give the program this thing we made 
    return uiArray
}

// this for node.js's require() function 
module.exports = UIArray