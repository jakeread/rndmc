// boilerplate rndmc header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI 

/*
collector

takes samples from what-ever output, stores in an array until a 'dump' input,
outputs that array, clears internals 

*/

// a constructor, a fn, a javascript mess
function Collector() {

    // this is the tiny program-as-and-object that we'll load into rundmc 
    // description / name is required to load successfully 
    var collector = {
        description: {
            name: 'collector',
            alt: 'collect and dump'
        }
    }

    // the State() object is what the system scrapes for ui variables / updates from the UI
    // this includes things like Button('title', callback), which are unique state variables
    // they can also be found in jsunit.js 
    collector.state = State()
    // alias !
    var state = collector.state 

    state.count = 0
    state.dumpOnGo = false 

    var collection = new Array()

    collector.ui = UI() 
    var ui = collector.ui 
    ui.addElement('onDumpButton', 'ui/uiButton.js')
    ui.onDumpButton.subscribe('onload', function(msg){
        ui.onDumpButton.send({
            calls: 'setText',
            argument: 'array out ->'
        })
    })
    ui.onDumpButton.subscribe('onclick', onDumpDesire)

    // inputs are required, and must be Input('type', callback) 
    collector.inputs = {
        collect: Input('any', onNewItem), // makes anything into num event 
        dump: Input('evt', onDumpDesire)
    }

    // outputs: Output('type')
    collector.outputs = {
        pass: Output('any')
    }

    // here's our input callback, specified in the input constructor 
    function onNewItem(input){
        collection.push(input)
        state.count = collection.length 
    }

    function onDumpDesire(){
        collector.outputs.pass.emit(collection)
        if(state.dumpOnGo){
            collection = new Array()
            state.count = collection.length 
        }   
    }

    // gotta give the program this thing we made 
    return collector
}

// this for node.js's require() function 
module.exports = Collector