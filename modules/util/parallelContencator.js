// boilerplate rndmc header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI

// a constructor, a fn, a *hot* javascript mess
function ParallelContencator() {

    // this is the tiny program-as-and-object that we'll load into rundmc 
    // description / name is required to load successfully 
    var parallelContencator = {
        description: {
            name: 'parallelContencator',
            alt: 'inputs -> arrays'
        }
    }

    // the State() object is what the system scrapes for ui variables / updates from the UI
    // this includes things like Button('title', callback), which are unique state variables
    // they can also be found in jsunit.js 
    parallelContencator.state = State()
    // alias !
    var state = parallelContencator.state

    state.gateKeep = true

    parallelContencator.ui = UI()
    var ui = parallelContencator.ui
    ui.addElement('onOutputButton', 'ui/uiButton.js')
    ui.onOutputButton.subscribe('onload', function(msg){
        ui.onOutputButton.send({
            calls: 'setText',
            argument: 'array out ->'
        })
    })
    ui.onOutputButton.subscribe('onclick', doThroughput)

    // inputs are required, and must be Input('type', callback) 
    parallelContencator.inputs = {
        in0: Input('any', onIn0),
        in1: Input('any', onIn1),
        push: Input('evt', doThroughput) // makes anything into num event 
    }

    // outputs: Output('type')
    parallelContencator.outputs = {
        out: Output('array')
    }

    var contencated = [0, 0]
    var recent = [false, false]

    function onIn0(inp) {
        contencated[0] = inp
        recent[0] = true
        doGateCheck()
    }

    function onIn1(inp) {
        contencated[1] = inp
        recent[1] = true
        doGateCheck()
    }

    function doGateCheck() {
        if (state.gateKeep) {
            var count = 0
            recent.forEach(function(element) {
                if (element) {
                    count++
                }
            })
            if (count == recent.length) {
                doThroughput()
            }
        } else {
            doThroughput()
        }
    }

    function doThroughput() {
        // here's how we fire an output. 
        parallelContencator.outputs.out.emit(contencated)
        recent.fill(false)
    }

    // gotta give the program this thing we made 
    return parallelContencator
}

// this for node.js's require() function 
module.exports = ParallelContencator