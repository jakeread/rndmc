// boilerplate header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI

// a constructor, a fn, a javascript mess
function Gate() {

    var gate = {
        // descriptions are used in UI
        description: {
            name: 'gate',
            alt: 'in ... out'
        }
    }

    gate.state = State()
    // alias ! 
    var state = gate.state

    state.message = 'closed'

    gate.ui = UI()
    var ui = gate.ui
    ui.addElement('openButton', 'ui/uiButton.js')
    ui.openButton.subscribe('onload', function(msg) {
        ui.openButton.send({
            calls: 'setText',
            argument: 'click to open gate'
        })
    })
    ui.openButton.subscribe('onclick', onButtonPress)

    // yikes 
    gate.isOpen = false

    gate.inputs = {
        thru: Input('any', gateKeeper) // makes anything into '1' event
    }

    gate.outputs = {
        out: Output('any')
    }

    function onButtonPress(evt) {
        if (gate.isOpen) {
            gate.isOpen = false
            state.message = 'closed'
            ui.openButton.send({
                calls: 'setText',
                argument: 'click to open gate'
            })
        } else {
            gate.isOpen = true
            state.message = 'open'
            ui.openButton.send({
                calls: 'setText',
                argument: 'click to close gate'
            })
        }
    }

    function gateKeeper(input) {
        // dereference for kicks
        var outVar = JSON.parse(JSON.stringify(input))
        if (gate.isOpen) {
            gate.outputs.out.emit(outVar)
        }
    }

    return gate
}

// exports 
module.exports = Gate