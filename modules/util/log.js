// boilerplate header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// a constructor, a fn, a javascript mess
function Logger() {

    var logger = {
        // descriptions are used in UI
        description: {
            name: 'logger',
            alt: 'in ... out to console'
        }
    }

    logger.state = State()
    // alias !
    var state = logger.state

    state.prefix = 'LOGGER:'
    state.message = '---'
    state.logToggle = true

    logger.inputs = {
        thru: Input('any', onInput) // makes anything into '1' event
    }

    logger.outputs = {
        throughput: Output('any')
    }

    function onInput(input) {
        if (state.logToggle) {
            state.message = input.toString()
            console.log(state.prefix, input)
        }
    }

    return logger
}

// exports 
module.exports = Logger