// boilerplate header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI

// a constructor, a fn, a javascript mess
function GateCounter() {

    var gateCounter = {
        // descriptions are used in UI
        description: {
            name: 'gateCounter',
            alt: 'in ... out'
        }
    }

    gateCounter.state = State()
    // alias ! 
    var state = gateCounter.state

    state.addThis = 10 
    state.count = 0

    gateCounter.ui = UI()
    var ui = gateCounter.ui
    ui.addElement('openButton', 'ui/uiButton.js')
    ui.openButton.subscribe('onload', function(msg){
        ui.openButton.send({
            calls: 'setText',
            argument: 'click to add ' +  state.addThis.toString() + ' to count'
        })
    })
    ui.openButton.subscribe('onclick', onButtonPress)

    state.onUiChange('addThis', function(){
        ui.openButton.send({
            calls: 'setText',
            argument: 'click to add ' +  state.addThis.toString() + ' to count'
        })
    })

    // yikes 
    gateCounter.isOpen = false

    gateCounter.inputs = {
        thru: Input('any', gateCounterKeeper), // makes anything into '1' event
        setCount: Input('number', onSetCount),
        addEvent: Input('any', onAddEvent)
    }

    gateCounter.outputs = {
        out: Output('any')
    }

    function onButtonPress(evt) {
        console.log("gateCounter BUTTON")
        state.count += state.addThis
    }

    function onSetCount(num){
        state.addThis = num 
        state.count = num 
    }

    function onAddEvent(evt){
        state.count = state.addThis 
    }

    function gateCounterKeeper(input) {
        if (state.count > 0) {
            var outVar = JSON.parse(JSON.stringify(input))
            gateCounter.outputs.out.emit(outVar)
            state.count -- 
        }
    }

    return gateCounter
}

// exports 
module.exports = GateCounter