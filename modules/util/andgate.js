// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI 

// a constructor, a fn, a javascript mess
function AndFlowControl() {

    var andGate = {
        // descriptions are used in UI
        description: {
            name: 'andGate',
            alt: 'in ... out'
        }
    }

    andGate.state = State()
    // alias !
    var state = andGate.state 

    // yikes 
    andGate.isOpen = false
    state.message = 'closed'

    andGate.ui = UI() 
    var ui = andGate.ui 
    ui.addElement('openButton', 'ui/uiButton.js')
    ui.openButton.subscribe('onload', function(msg){
        ui.openButton.send({
            calls: 'setText',
            argument: 'open / close'
        })
    })
    ui.openButton.subscribe('onclick', onButtonPress)

    andGate.inputs = {
        thru: Input('any', andGateKeeper) // makes anything into '1' event
    }

    andGate.outputs = {
        out: Output('any')
    }

    function onButtonPress(evt){
        state.toggle.isPressed = false
        if(andGate.isOpen){
            andGate.isOpen = false
            state.message = 'closed' 
        } else {
            andGate.isOpen = true
            state.message = 'open'
            andGate.outputs.out.emit('go')
        }
    }

    function andGateKeeper(input){
        // dereference for kicks
        var outVar = JSON.parse(JSON.stringify(input))
        if(andGate.isOpen){
            andGate.outputs.out.emit(outVar)
        }
    }

    return andGate
}

// exports 
module.exports = AndFlowControl