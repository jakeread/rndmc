// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

// interface elements
const JSUI = require('../../src/jsui.js')
let UI = JSUI.UI

// the 'andFlow' flowcontrol unit only lets events through once both have occurred ... 

function AndFlow() {

    var andFlow = {
        // descriptions are used in UI
        description: {
            name: 'andFlow',
            alt: 'in ... out'
        }
    }

    andFlow.state = State()
    // alias !
    var state = andFlow.state
    // yikes 
    state.A = 0
    state.B = 0

    andFlow.ui = UI()
    var ui = andFlow.ui
    ui.addElement('btnReset', 'ui/uiButton.js')
    ui.btnReset.subscribe('onload', function(msg){
        ui.btnReset.send({
            calls: 'setText',
            argument: 'reset'
        })
    })
    ui.btnReset.subscribe('onclick', onReset)

    andFlow.inputs = {
        reset: Input('any', onReset),
        A: Input('any', onA),
        B: Input('any', onB)
    }

    andFlow.outputs = {
        out: Output('any')
    }

    function onReset(evt) {
        state.A = 0
        state.B = 0
    }

    function onA(input) {
        state.A = 1
        if (state.A && state.B) {
            andFlow.outputs.out.emit(1)
            onReset()
        }
    }

    function onB(input) {
        state.B = 1
        if (state.A && state.B) {
            andFlow.outputs.out.emit(1)
            onReset()
        }
    }

    return andFlow
}

// exports 
module.exports = AndFlow