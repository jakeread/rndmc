/* Easiest way I know of is to use "child_process" package which comes packaged with node.

Then you can do something like: */
const JSUnit = require('../../src/jsunit.js')
const MJS = require('mathjs')
const spawn = require("child_process").spawn
//const express = require('express')
//const app = express()

let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

function leastSquares() {
    var thetas = 0
    var theta1s = []
    var theta2s = []
    
    var leastSquares = {
        description: {
            name: 'leastSquares Parser',
            alt: 'line of leastSquares -> l1 and l2 values'
        }
    }

    // log more 
    // var verbose = false 

    // one caveat here is that we can't dynamically add objects to this, 
    // or they don't get getter / settered when we do
    leastSquares.state = State()
    leastSquares.state.c = 1
    leastSquares.state.d = -2
    // leastSquares.state.G1 = 400

    leastSquares.inputs = {
        thetas: Input('any', intakeThetas), // can be taken in as a string
        //theta2s: Input('any', intakeTheta2s)
    }

    leastSquares.outputs = {
        l1: Output('num'),
        l2: Output('num')
        // instructionOut: Output('move instruction'),
        // modeChange: Output('string')
    }

    function intakeThetas(list){
        makeTheta1Theta2(list)
        console.log(theta1s, theta2s)
        var lengths = parseleastSquares()
    }

    function makeTheta1Theta2(list){
        var n = list.length
        var i;
        for (i = 0; i < n; i++)
        {
            theta1s.push(list[i][0])
            theta2s.push(list[i][1])
        }
    }

    /*
    function intakeTheta2s(list){
        theta2s = list
        var lengths = parseleastSquares()
        //console.log('theta2 input')
    }
    */

    /*
    function thetasToStrings(list){

    }
    */

    function parseleastSquares() {
        var lengths = {
            l1: 0,
            l2: 0//,
            //test: 'something'
        }

        theta1sString = theta1s.toString()
        theta2sString = theta2s.toString()
        cString = leastSquares.state.c.toString()
        dString = leastSquares.state.d.toString()
        //console.log("we here at least:"+theta1sString)
        
        const pythonProcess = spawn('python',['py/leastSquares.py', theta1sString, theta2sString, cString, dString])
        //console.log('do we get here?')
        //console.log(pythonProcess)

        pythonProcess.stdout.on('data', (data) => {
            //console.log('got here yay python!!')
            //console.log(data.toString())
            var temp = JSON.parse(data.toString())
            console.log(temp)
            lengths.l1 = temp[0]
            lengths.l2 = temp[1]
            console.log(typeof(lengths.l1))
            console.log(lengths.l1,lengths.l2)
            //lengths.test = data.toString()
            leastSquares.outputs.l1.emit(lengths.l1)
            leastSquares.outputs.l2.emit(lengths.l2)
        })

        pythonProcess.stderr.on('data', function(data){
            console.log("Error: " + data);
        });

        /*
        app.get('/', (req, res) => {
            const { spawn } = require('child_process');
            const pythonProcess = spawn('python',['leastSquares.py', theta1s, theta2s, c, d]);

            pythonProcess.stdout.on('data', function(data) {

                console.log(data.toString());
                res.write(data);
                res.end('end');
            });
        })

        app.listen(4000, () => console.log('Application listening on port 4000!'))    
        */

        /* var instruction = {
            position: {},
            hasMove: false,
            speed: 0
        }

        kv = getKeyValues(str)
        // track modality
        if (kv.G == 0 | kv.G == 1) {
            leastSquares.state.mode = 'G' + kv.G.toString()
        } else if (kv.G != null) {
            // no arcs pls
            console.log('unfriendly leastSquares mode!', kv)
        }

        for (key in kv) {
            if (key.match('[A-EX-Z]')) {
                instruction.position[key] = kv[key]
                instruction.hasMove = true
            } else if (key.match('[F]')) {
                // tricky / ugly: sets using the mode state string as object key 
                leastSquares.state[leastSquares.state.mode] = kv.F
            }
        }

        instruction.speed = leastSquares.state[leastSquares.state.mode]
        // and this for help later?
        instruction.kv = kv
        */
        return lengths
    }

    return leastSquares

}


// export the module 
module.exports = leastSquares