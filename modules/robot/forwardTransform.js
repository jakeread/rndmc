// boilerplate atkapi header
const JSUnit = require('../../src/jsunit.js')
const MJS = require('mathjs')

let Input = JSUnit.Input
let Output = JSUnit.Output
let State = JSUnit.State

function forwardTransform() {
    var forwardTransform = {
        description: {
            name: 'forwardTransform Parser',
            alt: 'line of forwardTransform -> points'
        }
    }

    // log more 
    var verbose = false 

    // one caveat here is that we can't dynamically add objects to this, 
    // or they don't get getter / settered when we do
    forwardTransform.state = State()
    var state = forwardTransform.state
    state.tA = 0 
    state.tB = 0
    state.tC = 0 
    state.lA = 0.4
    state.lB = 0.2
    state.onUiChange('tA', () => {
        doForwardTransform()
    })

    forwardTransform.inputs = {
        theta_A: Input('any', (num) => {
            state.tA = num
            doForwardTransform()
        }),
        theta_B: Input('any', (num) => {
            state.tB = num
            doForwardTransform()
        }),
        theta_C: Input('number', (num) => {
            state.tC = num
            doForwardTransform()
        }),
        len_A: Input('any', (num) => {
            state.lA = num
            doForwardTransform()
        }),
        len_B: Input('any', (num) => {
            state.lB = num
            doForwardTransform()
        })
    }

    forwardTransform.outputs = {
        ptA: Output('array'),
        ptB: Output('array'),
        ptC: Output('array')
    }

    // TODO: test, can we link global vars to ui objects ... 
    // forwardTransform.ui.mode.value = var ? no bc set / get etc 
    // more like var = forwardTransform.ui.mode.value ? is this referential?

    function doForwardTransform() {
        var lenA = state.lA*Math.cos(state.tB)
        var lenB = state.lB*Math.cos(state.tB + state.tC)

        var knuckle = [ lenA*Math.cos(state.tA), 
                        lenA*Math.sin(state.tA), 
                        state.lA*Math.sin(state.tB)]

        var tip = [ knuckle[0] + lenB*Math.cos(state.tA),
                    knuckle[1] + lenB*Math.sin(state.tA),
                    knuckle[2] + state.lB*Math.sin(state.tB + state.tC)]
        /*
        [     state.lA*Math.cos(state.tA) + state.lB*Math.cos(state.tA + state.tB), 
                        state.lA*Math.sin(state.tA) + state.lB*Math.sin(state.tA + state.tB), 
                        state.lA*Math.sin(state.tB) + state.lB*Math.sin(state.tA + state.tB)]
        */

        forwardTransform.outputs.ptA.emit([0,0,0])
        forwardTransform.outputs.ptB.emit(knuckle)
        forwardTransform.outputs.ptC.emit(tip)
    }

    return forwardTransform

}


// export the module 
module.exports = forwardTransform