
// business 
const Reps = require('./reps.js')
const Programs = require('./programs.js')

// the program object: real simple, just has a description, and a 'modules' 
var program = Programs.new('new program')

/* ok

- robot reads robot joints
- robot takes joint angle samples into array like
	[[t1, t2],[t1, t2],...,[t1,t2]]
- robot sends this array to python scripto 
- python scripto does l1, l2 guesses
- robot does forward transform with live [t1, t2]
- robot displays forward transform with [t1, t2]

*/


var link = Programs.loadModuleFromSource(program, './modules/hardware/atkseriallink.js')
link.startUp()
link.state.log = false 
Programs.setUI(link, 1050, 150)

var mrbotone = Programs.loadModuleFromSource(program, './modules/hardware/atkmrobot.js')
mrbotone.route.route = '0,0'
mrbotone.state.enc_reverse = true 
mrbotone.state.enc_offset = 8875 
Programs.setUI(mrbotone, 600, 50)

var mrbottwo = Programs.loadModuleFromSource(program, './modules/hardware/atkmrobot.js')
mrbottwo.state.enc_reverse = true 
mrbottwo.state.enc_offset = 4000 
mrbottwo.route.route = '0,1'
Programs.setUI(mrbottwo, 600, 550)

var mrbotthree = Programs.loadModuleFromSource(program, './modules/hardware/atkmrobot.js')
mrbotthree.route.route = '0,2'
mrbotthree.state.enc_reverse = false 
mrbotthree.state.enc_offset = 1700 
Programs.setUI(mrbotthree, 600, 1050)

var rbtcanvas = Programs.loadModuleFromSource(program, './modules/ui/robotCanvas.js')
Programs.setUI(rbtcanvas, 1500, 1050)

Programs.setView(program, {
	scale: 0.5,
	translate: [-160, -30],
	origin: [200, 120]
})

var button = Programs.loadModuleFromSource(program, './modules/ui/button.js')
var delay = Programs.loadModuleFromSource(program, './modules/util/delay.js')
var gate = Programs.loadModuleFromSource(program, './modules/util/gate.js')
//delay.outputs.out.attach(gate.inputs.thru)
//gate.outputs.out.attach(delay.inputs.thru)
Programs.setUI(button, 90, 50)
Programs.setUI(delay, 90, 250)
Programs.setUI(gate, 90, 400)

button.outputs.whammy.attach(mrbotone.inputs.get_pos)
button.outputs.whammy.attach(mrbottwo.inputs.get_pos)
button.outputs.whammy.attach(mrbotthree.inputs.get_pos)
button.outputs.whammy.attach(delay.inputs.thru)
delay.outputs.out.attach(gate.inputs.thru)
gate.outputs.out.attach(button.inputs.thru)

var transform = Programs.loadModuleFromSource(program, './modules/robot/forwardTransform.js')
transform.state.lA = 0.37
transform.state.lB = 0.25
Programs.setUI(transform, 1225, 650)

var log1 = Programs.loadModuleFromSource(program, './modules/util/log.js')
log1.state.prefix = "jnt1:"
Programs.setUI(log1, 1500, 50)
mrbotone.outputs.pos.attach(log1.inputs.thru)
mrbotone.outputs.pos.attach(transform.inputs.theta_A)

var log2= Programs.loadModuleFromSource(program, './modules/util/log.js')
log2.state.prefix = "jnt2:"
Programs.setUI(log2, 1500, 250)
mrbottwo.outputs.pos.attach(log2.inputs.thru)
mrbottwo.outputs.pos.attach(transform.inputs.theta_B)

var log3 = Programs.loadModuleFromSource(program, './modules/util/log.js')
log3.state.prefix = "jnt3:"
Programs.setUI(log3, 1500, 450)
mrbotthree.outputs.pos.attach(log3.inputs.thru)
mrbotthree.outputs.pos.attach(transform.inputs.theta_C)

transform.outputs.ptA.attach(rbtcanvas.inputs.xy1)
transform.outputs.ptB.attach(rbtcanvas.inputs.xy2)
transform.outputs.ptC.attach(rbtcanvas.inputs.xy3)

/*

var collector = Programs.loadModuleFromSource(program, './modules/util/collector.js')
Programs.setUI(collector, 1050, 800)

var gateCounter = Programs.loadModuleFromSource(program, './modules/util/gateCounter.js')
Programs.setUI(gateCounter, 600, 850)

*/

/*
var stest = Programs.loadModuleFromSource(program, './modules/ui/stest.js')

var rep = Reps.makeFromModule(stest)
console.log1('rep', rep)

/* example program-like-an-api 
// load some modules
var multiline = Programs.loadModuleFromSource(program, './modules/ui/multiline.js')
var gcode = Programs.loadModuleFromSource(program, './modules/parsing/gcode.js')

// attaching: always like outputs to inputs
multiline.outputs.lineOut.attach(gcode.inputs.lineIn)

// we can move things around here as well
multiline.description.position = {
	left: 50,
	top: 50
}

gcode.description.position = {
	left: 500,
	top: 100
}

// if I have a public function in a module, I can also use that
multiline.load('./files/dogbone.gcode')
*/

// UI 
const View = require('./views.js')
View.startHttp() 
View.startWs() 

Programs.assignSocket(View.uiSocket)
View.assignProgram(program)