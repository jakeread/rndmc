//
//
// new node controller / HEAP 
// reconfigurable numeric dataflow machine controller / RNDMC
//
// main.js
//
//
// Jake Read at the Center for Bits and Atoms
// (c) Massachusetts Institute of Technology 2018
//
// This work may be reproduced, modified, distributed, performed, and
// displayed for any purpose, but must acknowledge the mods
// project. Copyright is retained and must be preserved. The work is
// provided as is; no warranty is provided, and users accept all
// liability.

/*

RULES:

modules are objects having inputs, outputs, and state 
programs are assemblies of modules 

*/

// business 
const Reps = require('./reps.js')
const Programs = require('./programs.js')

// the program object: real simple, just has a description, and a 'modules' 
var program = Programs.new('new program')

//var program = Programs.open('./programs/temp.json')

/*
var stest = Programs.loadModuleFromSource(program, './modules/ui/stest.js')

var rep = Reps.makeFromModule(stest)
console.log('rep', rep)

/* example program-like-an-api 
// load some modules
var multiline = Programs.loadModuleFromSource(program, './modules/ui/multiline.js')
var gcode = Programs.loadModuleFromSource(program, './modules/parsing/gcode.js')

// attaching: always like outputs to inputs
multiline.outputs.lineOut.attach(gcode.inputs.lineIn)

// we can move things around here as well
multiline.description.position = {
	left: 50,
	top: 50
}

gcode.description.position = {
	left: 500,
	top: 100
}

// if I have a public function in a module, I can also use that
multiline.load('./files/dogbone.gcode')
*/

// UI 
const View = require('./views.js')
View.startHttp() 
View.startWs() 

Programs.assignSocket(View.uiSocket)
View.assignProgram(program)