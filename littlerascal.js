// business 
const Reps = require('./reps.js')
const Programs = require('./programs.js')

// the program object: real simple, just has a description, and a 'modules' 
var program = Programs.new('new program')

/* ok
- video
 - cut: red
 - mountain: green
 - valley: blue 
- git push
*/

// das link 
var link = Programs.loadModuleFromSource(program, './modules/hardware/atkseriallink.js')
link.startUp()
link.state.log = false

let steps = 80

// x motor
var xm = Programs.loadModuleFromSource(program, './modules/hardware/atkstepper.js')
xm.route.route = '0,0'
xm.state.route = '0,0'
xm.state.spu = steps
xm.state.axis = 'X'

// y left and right
var ylm = Programs.loadModuleFromSource(program, './modules/hardware/atkstepper.js')
ylm.route.route = '0,1'
ylm.state.route = '0,1'
ylm.state.spu = steps 
ylm.state.axis = 'Y'

var ylr = Programs.loadModuleFromSource(program, './modules/hardware/atkstepper.js')
ylr.route.route = '0,2'
ylr.state.route = '0,2'
ylr.state.spu = -steps
ylr.state.axis = 'Y'

// z motor
var zm = Programs.loadModuleFromSource(program, './modules/hardware/atkstepper.js')
zm.route.route = '0,3'
zm.state.route = '0,3'
zm.state.spu = -steps
zm.state.axis = 'Z'

var planner = Programs.loadModuleFromSource(program, './modules/motion/simplanner.js')
planner.state.minSpeed = 0.1
planner.state.accel = 20

var outButton = Programs.loadModuleFromSource(program, './modules/motion/usermove.js')
outButton.state.moveSpeed = 200

/*

var simplanner = Programs.loadModuelFromSource(program, './modules/motion/simplanner.js')

let ahMove = {
	axes: ['X', 'Y', 'Z'],
	p1: [10, 10, 10],
	p2: [20, 20, 20],
	vector: [10, 10, 10],
	cruise: 100,
	entry: 10,
	exit: 10, 
	accel: 100
}

*/

// what doth planner output ?
/*
var newMove = {
    axes: axes,
    p1: p1,
    p2: p2,
    cruise: move.speed,
    entry: 0,
    exit: 0,
    accel: state.accel
}
*/

// can we write something that just steps through moves one by one ? 

// HOOKUP
planner.outputs.moves.attach(xm.inputs.trapezoid)
planner.outputs.moves.attach(ylm.inputs.trapezoid)
planner.outputs.moves.attach(ylr.inputs.trapezoid)
planner.outputs.moves.attach(zm.inputs.trapezoid)

xm.outputs.ack.attach(planner.inputs.acks)
xm.outputs.ack.attach(planner.inputs.positions)

ylm.outputs.ack.attach(planner.inputs.acks)
ylm.outputs.ack.attach(planner.inputs.positions)
ylr.outputs.ack.attach(planner.inputs.acks)

zm.outputs.ack.attach(planner.inputs.acks)
zm.outputs.ack.attach(planner.inputs.positions)

let moveToPlanner = {
	position: {
		X: 20,
		Y: 10,
		Z: 5
	},
	speed: 150
}

link.onOpen = function(){
	// planner.inputs.instruction.fn(moveToPlanner)
	//xm.inputs.trapezoid.fn(ahMove)
}

// ws input
// fusion runs on socket 1234 ... 

var sockit = Programs.loadModuleFromSource(program, './modules/pipes/websocket.js')
var np = Programs.loadModuleFromSource(program, './modules/motion/neilparse.js')

sockit.outputs.data.attach(np.inputs.array)
np.outputs.move.attach(planner.inputs.instruction)
planner.outputs.moveComplete.attach(np.inputs.shift)

outButton.outputs.move.attach(planner.inputs.instruction)

// UI HOOKUP

Programs.setView(program, {
    scale: 0.5,
    translate: [-160, -30],
    origin: [200, 120]
})

let top = 100
let motorbar = 1200
let motorspace = 400

Programs.setUI(sockit, top, top)
Programs.setUI(np, 600, top)
Programs.setUI(outButton, top, 300)

Programs.setUI(link, 1800, top)
Programs.setUI(xm, motorbar, top)
Programs.setUI(ylm, motorbar, top + motorspace)
Programs.setUI(ylr, motorbar, top + 2 * motorspace)
Programs.setUI(zm, motorbar, top + 3 * motorspace)
Programs.setUI(planner, 600, 450)

// UI 
const View = require('./views.js')
View.startHttp()
View.startWs()

Programs.assignSocket(View.uiSocket)
View.assignProgram(program)