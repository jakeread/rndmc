
## Writing Javascript Programs

There's also something of an 'interface' to the software, and if you prefer to write a headless controller, or roll your own UI, etc, this is fair game. Please, break my software and use it in wierd ways. 

The only code in ```main.js``` that we need to run headless is here:

```JavaScript 
const Reps = require('./reps.js')
const Programs = require('./programs.js')

var program = Programs.new('new program')
``` 

We don't need the next lines (opening a View and starting an Http server). I.E. to add new modules and hook them up, try this:

```JavaScript
var stepper = Programs.loadModuleFromSource(program, './modules/hardware/atkstepper.js')
var jogControl = Programs.loadModuleFromSource(program, './modules/ui/arrows.js')
``` 